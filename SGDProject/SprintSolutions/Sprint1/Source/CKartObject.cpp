#include "CKartObject.h"
#include "CGamePlayState.h"

CKartObject::CKartObject() : CBase() {
	m_nWeight = 10.0f;
	m_nFriction = 30.0f;
	m_nAccelerationRate = 50.0;
	m_nRotationRate = 3.14f;
	m_nCurSpeed = 0.0f;
	m_nMaxSpeed = 150.0;
	m_nCurRotation = 0.0f;
	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;

	m_bAccelerating = false;
}

void CKartObject::RotateLeft() {
	m_nCurRotation -= m_nRotationRate * CTimer::GetInstance()->GetDelta();
}

void CKartObject::RotateRight() {
	m_nCurRotation += m_nRotationRate * CTimer::GetInstance()->GetDelta();
}

void CKartObject::Update(float nDelta) {
	if (IsAccelerating()) {
		if (m_nCurSpeed < m_nMaxSpeed) {
			m_nCurSpeed += m_nAccelerationRate * nDelta;
		} else if (m_nCurSpeed > m_nMaxSpeed)
			m_nCurSpeed = m_nMaxSpeed;
	} else if (IsDecelerating()) {
		if (m_nCurSpeed > 0)
			m_nCurSpeed -= m_nAccelerationRate * 2 * nDelta;
	} else {
		if (m_nCurSpeed > 0) {
			m_nCurSpeed -= m_nAccelerationRate * nDelta;
		}
	}
	if (m_nCurSpeed < 0)
		m_nCurSpeed = 0;

	m_nCurSpeed -= m_nWeight * nDelta;

	static int nBoundaries;
	static int nOffroad;
	
	nBoundaries = CGamePlayState::GetInstance()->GetBoundaries().size();
	nOffroad = CGamePlayState::GetInstance()->GetOffroad().size();
	
	for (int i = 0; i < nOffroad; ++i) {
		if (CGamePlayState::GetInstance()->GetOffroad()[i].CheckCollision((CBase*)this)) {
			m_nCurSpeed -= m_nFriction * nDelta;
			break;
		}
	}
	
	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;
	m_vThrust = Vector2DRotate(m_vThrust, m_nCurRotation);
	m_vThrust = m_vThrust * m_nCurSpeed;

	for (int i = 0; i < nBoundaries; ++i) {
		if (CGamePlayState::GetInstance()->GetBoundaries()[i].CheckCollision((CBase*)this)) {
			//m_nCurSpeed *= -1;
			m_vThrust.fX *= -5;
			m_vThrust.fY *= -5;
			break;
		}
	}

	SetXVelocity(m_vThrust.fX);
	SetYVelocity(m_vThrust.fY);
	
	CBase::Update(nDelta);
}

void CKartObject::Render() {
	static tVector2D vAngle;
	vAngle.fX = _ORIENTATION_X_;
	vAngle.fY = _ORIENTATION_Y_;
	vAngle = Vector2DRotate(vAngle, m_nCurRotation);
	vAngle = vAngle * 10;

	CSGD_Direct3D::GetInstance()->DrawRect(GetCollisionRect(), 255, 0, 0);
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition(), GetYPosition(), GetXPosition() + vAngle.fX, GetYPosition() + vAngle.fY, 255, 255, 255);
}

RECT CKartObject::GetCollisionRect() {
	RECT rTemp;
	SetRect(&rTemp, (GetXPosition() - GetWidth()/2), (GetYPosition() - GetHeight()/2), (GetXPosition() - GetWidth()/2) + GetWidth(), (GetYPosition() - GetHeight()/2) + GetHeight());
	return rTemp;
}