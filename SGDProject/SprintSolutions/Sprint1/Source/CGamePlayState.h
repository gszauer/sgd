#ifndef _H_CGAMEPLAYSTATE_
#define _H_CGAMEPLAYSTATE_

#include "CGame.h"
#include "CTimer.h"
#include "IGameState.h"
#include "CKartObject.h"
#include "CBoundaryObject.h"
#include "COffroadObject.h"
#include "SGDWrappers/CSGD_Direct3D.h"
#include "SGDWrappers/CSGD_ObjectManager.h"
#include <vector>
using std::vector;

class CGamePlayState : public IGameState {
private:
	unsigned char			m_ucAccelerate;
	unsigned char			m_ucDecelerate;
	unsigned char			m_ucLeft;
	unsigned char			m_ucRight;
	unsigned char			m_ucItem;

	CKartObject m_Player;
	vector<CBoundaryObject> m_vBoundarys;
	vector<COffroadObject> m_vOffroad;
	RECT rScreen; // TEMP

	CGamePlayState();
	~CGamePlayState() {}
	CGamePlayState(const CGamePlayState& rCGamePlayState) {}
	CGamePlayState& operator=(const CGamePlayState& rCGamePlayState) {}
public:
	static CGamePlayState* GetInstance();
	vector<CBoundaryObject>& GetBoundaries();
	vector<COffroadObject>& GetOffroad();
	void Reset();
	void Enter();
	bool Input();
	void Update(float fDelta);
	void Render();
	void Exit();
};

#endif