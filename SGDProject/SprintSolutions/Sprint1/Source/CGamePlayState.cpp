#include "CGAmePlayState.h"

CGamePlayState::CGamePlayState() {
	CBoundaryObject	boundaries[4];
	COffroadObject	offroad[17];
	m_Player = CKartObject();
	m_Player.SetXPosition(90);
	m_Player.SetYPosition(288);
	m_Player.SetWidth(18);
	m_Player.SetHeight(18);

	m_ucAccelerate = DIK_W;
	m_ucDecelerate = DIK_S;
	m_ucLeft = DIK_A;
	m_ucRight = DIK_D;
	m_ucItem = DIK_SPACE;


	// Set up stage boundaries
	boundaries[0].SetXPosition(0);
	boundaries[0].SetYPosition(0);
	boundaries[0].SetWidth(640);
	boundaries[0].SetHeight(18);

	boundaries[1].SetXPosition(0);
	boundaries[1].SetYPosition(450);
	boundaries[1].SetWidth(640);
	boundaries[1].SetHeight(30);

	boundaries[2].SetXPosition(0);
	boundaries[2].SetYPosition(18);
	boundaries[2].SetWidth(18);
	boundaries[2].SetHeight(432);

	boundaries[3].SetXPosition(612);
	boundaries[3].SetYPosition(18);
	boundaries[3].SetWidth(28);
	boundaries[3].SetHeight(432);

	// Set up offroad objects
	offroad[0].SetXPosition(18);
	offroad[0].SetYPosition(18);
	offroad[0].SetWidth(54);
	offroad[0].SetHeight(432);

	offroad[1].SetXPosition(576);
	offroad[1].SetYPosition(18);
	offroad[1].SetWidth(36);
	offroad[1].SetHeight(432);

	offroad[2].SetXPosition(72);
	offroad[2].SetYPosition(18);
	offroad[2].SetWidth(504);
	offroad[2].SetHeight(72);

	offroad[3].SetXPosition(72);
	offroad[3].SetYPosition(414);
	offroad[3].SetWidth(504);
	offroad[3].SetHeight(36);

	offroad[4].SetXPosition(72);
	offroad[4].SetYPosition(90);
	offroad[4].SetWidth(36);
	offroad[4].SetHeight(18);

	offroad[5].SetXPosition(540);
	offroad[5].SetYPosition(90);
	offroad[5].SetWidth(36);
	offroad[5].SetHeight(18);

	offroad[6].SetXPosition(72);
	offroad[6].SetYPosition(396);
	offroad[6].SetWidth(36);
	offroad[6].SetHeight(18);

	offroad[7].SetXPosition(540);
	offroad[7].SetYPosition(396);
	offroad[7].SetWidth(36);
	offroad[7].SetHeight(18);

	offroad[8].SetXPosition(72);
	offroad[8].SetYPosition(108);
	offroad[8].SetWidth(18);
	offroad[8].SetHeight(18);

	offroad[9].SetXPosition(558);
	offroad[9].SetYPosition(108);
	offroad[9].SetWidth(18);
	offroad[9].SetHeight(18);

	offroad[10].SetXPosition(72);
	offroad[10].SetYPosition(378);
	offroad[10].SetWidth(18);
	offroad[10].SetHeight(18);

	offroad[11].SetXPosition(558);
	offroad[11].SetYPosition(378);
	offroad[11].SetWidth(18);
	offroad[11].SetHeight(18);

	offroad[12].SetXPosition(144);
	offroad[12].SetYPosition(162);
	offroad[12].SetWidth(360);
	offroad[12].SetHeight(180);

	offroad[13].SetXPosition(162);
	offroad[13].SetYPosition(144);
	offroad[13].SetWidth(324);
	offroad[13].SetHeight(18);

	offroad[14].SetXPosition(126);
	offroad[14].SetYPosition(180);
	offroad[14].SetWidth(18);
	offroad[14].SetHeight(144);

	offroad[15].SetXPosition(162);
	offroad[15].SetYPosition(342);
	offroad[15].SetWidth(324);
	offroad[15].SetHeight(18);

	offroad[16].SetXPosition(504);
	offroad[16].SetYPosition(180);
	offroad[16].SetWidth(18);
	offroad[16].SetHeight(144);

	for (int i = 0; i < 4; ++i) {
		m_vBoundarys.push_back(boundaries[i]);
		//CSGD_ObjectManager::GetInstance()->AddObject((CBase*)&boundaries[i]);
	}

	for (int i  = 0; i < 17; ++i) {
		m_vOffroad.push_back(offroad[i]);
		//CSGD_ObjectManager::GetInstance()->AddObject((CBase*)&offroad[i]);
	}
}

vector<CBoundaryObject>& CGamePlayState::GetBoundaries() {
	return m_vBoundarys;
}

vector<COffroadObject>& CGamePlayState::GetOffroad() {
	return m_vOffroad;
}

CGamePlayState* CGamePlayState::GetInstance() {
 static CGamePlayState gamePlayState;
 return &gamePlayState;
}

void CGamePlayState::Reset() {

}

void CGamePlayState::Enter() {
	int r = CGame::GetInstance()->GetScreenWidth();
	int b = CGame::GetInstance()->GetScreenHeight();
	SetRect(&rScreen, 0, 0, r, b);
}

bool CGamePlayState::Input() {
	m_Player.SetAccelerating(CSGD_DirectInput::GetInstance()->KeyDown(m_ucAccelerate));
	m_Player.SetDecelerating(CSGD_DirectInput::GetInstance()->KeyDown(m_ucDecelerate));

	if (CSGD_DirectInput::GetInstance()->KeyDown(m_ucLeft)) {
		m_Player.RotateLeft();
		return true;
	} else if (CSGD_DirectInput::GetInstance()->KeyDown(m_ucRight)) {
		m_Player.RotateRight();
		return true;
	}

	return true;
}

void CGamePlayState::Update(float fDelta) {
	m_Player.Update(fDelta);
}

void CGamePlayState::Render() {
	CSGD_Direct3D::GetInstance()->DrawRect(rScreen, 0, 255, 255);
	for (int i = 0; i < 4; ++i)
		m_vBoundarys[i].Render();
	for (int i = 0; i < 17; ++i)
		m_vOffroad[i].Render();
	
	m_Player.Render();
	
	// Debug info
	CSGD_Direct3D::GetInstance()->SpriteEnd();
	char buffer1[255];
	sprintf(buffer1, "Game Time: %.5f", CTimer::GetInstance()->GetGameTime());
	char buffer2[255];
	sprintf(buffer2, "Delta Time: %.5f", CTimer::GetInstance()->GetDelta());
	char buffer3[255];
	sprintf(buffer3, "FPS: %.0f", CTimer::GetInstance()->GetFps());
	CSGD_Direct3D::GetInstance()->DrawText(buffer1, 0, 0);
	CSGD_Direct3D::GetInstance()->DrawText(buffer2, 0, 20);
	CSGD_Direct3D::GetInstance()->DrawText(buffer3, 0, 40);
	CSGD_Direct3D::GetInstance()->SpriteBegin();
}

void CGamePlayState::Exit() {

}