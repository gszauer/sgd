#ifndef _H_CBOUNDARYOBJECT_
#define _H_CBOUNDARYOBJECT_

#include <windows.h>
#include "CBaseObject.h"

class CBoundaryObject : public CBase{
public:
	CBoundaryObject(int nX = 0, int nY = 0, int nW = 1, int nH = 1);
	~CBoundaryObject() {}
	void Render();
};

#endif