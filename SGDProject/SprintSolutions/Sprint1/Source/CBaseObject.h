#ifndef _H_CBASEOBJECT_
#define _H_CBASEOBJECT_

#include "SGDWrappers/SGD_Math.h"
#include "SGDWrappers/CSGD_TextureManager.h"
#include "SGDWrappers/CSGD_Direct3D.h"
#include "SGDWrappers/CSGD_DirectInput.h"
#include <windows.h>

enum ObjectTypes {OBJ_BASE, OBJ_CART, OBJ_BOUNDARY, OBJ_MAX};

class CBase {
private:
	tVector2D	 m_vPosition;
	tVector2D	 m_vVelocity;
	tVector2D	 m_vSize;
	int			 m_nTextureId;
	unsigned int m_nRefCount;
	bool		 m_bIsActive;
protected:
	int			 m_nType;
public:
	CBase();
	virtual ~CBase() {}
	virtual void Update(float nDelta);
	virtual void Render();
	void AddRef();
	void Release();
	virtual RECT GetCollisionRect();
	virtual bool CheckCollision(CBase* pBase);

	// Accessors
	float GetXPosition() {return m_vPosition.fX;}
	float GetYPosition() {return m_vPosition.fY;}
	tVector2D GetPositionVector() {return m_vPosition;}
	tVector2D& GetPositionVectorRef() {return m_vPosition;}
	float GetXVelocity() {return m_vVelocity.fX;}
	float GetYVelocity() {return m_vVelocity.fY;}
	tVector2D GetVelocityVector() {return m_vVelocity;}
	tVector2D& GetVelocityVectorRef() {return m_vVelocity;}
	float GetWidth() {return m_vSize.fX;}
	float GetHeight() {return m_vSize.fY;}
	int GetTextureId() {return m_nTextureId;}
	bool IsActive() {return m_bIsActive;}
	int GetType() {return m_nType;}

	// Mutators
	void SetXPosition(float nX) {m_vPosition.fX = nX;}
	void SetYPosition(float nY) {m_vPosition.fY = nY;}
	void SetPositionVector(tVector2D vPosition) {m_vPosition = vPosition;}
	void SetXVelocity(float nX) {m_vVelocity.fX = nX;}
	void SetYVelocity(float nY) {m_vVelocity.fY = nY;}
	void SetVelocityVector(tVector2D vVelocity) {m_vVelocity = vVelocity;}
	void SetWidth(float nW) {m_vSize.fX = nW;}
	void SetHeight(float nH) {m_vSize.fY = nH;}
	void SetTextureId(int nId) {m_nTextureId = nId;}
	void SetActive(bool bActive) {m_bIsActive = bActive;}
};

#endif