#ifndef _H_COFFROADOBJECT_
#define _H_COFFROADOBJECT_

#include <windows.h>
#include "CBaseObject.h"

class COffroadObject : public CBase {
private: 
	RECT m_rObject;
public:
	COffroadObject(int nX = 0, int nY = 0, int nW = 1, int nH = 1);
	~COffroadObject() {}
	void Render();
};

#endif