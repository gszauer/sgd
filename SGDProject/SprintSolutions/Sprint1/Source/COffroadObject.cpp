#include "COffroadObject.h"

COffroadObject::COffroadObject(int nX, int nY, int nW, int nH) : CBase() {
	SetXPosition(nX);
	SetYPosition(nY);
	SetWidth(nW);
	SetHeight(nH);
	m_nType = OBJ_BOUNDARY;
}

void COffroadObject::Render() {
	CSGD_Direct3D::GetInstance()->DrawRect(GetCollisionRect(), 0, 125, 125);
}