#ifndef _H_CKARTOBJECT_
#define _H_CKARTOBJECT_

#include "CBaseObject.h"
#include "CTimer.h"

#define _ORIENTATION_X_ 0
#define _ORIENTATION_Y_ -1

class CKartObject : public CBase {
private:
	tVector2D	m_vThrust; // Rotation + speed = thrust

	float		m_nWeight;
	float		m_nFriction;
	float		m_nAccelerationRate;
	float		m_nRotationRate;
	float		m_nCurRotation;
	float		m_nCurSpeed;
	float		m_nMaxSpeed;
	
	bool		m_bAccelerating;
	bool		m_bDecelerating;

	// Pointer to current item
	// Cheats
public:
	CKartObject();
	~CKartObject() {}
	void Render();
	RECT GetCollisionRect();

	void RotateLeft();
	void RotateRight();

	void Update(float nDelta);
	float GetWeight() {return m_nWeight;}
	float GetFriction() {return m_nFriction;}
	float GetCurSpeed() {return m_nCurSpeed;}
	float GetMaxSpeed() {return m_nMaxSpeed;}
	bool CanAccelerate() {return (m_nCurSpeed < m_nMaxSpeed);}
	bool IsAccelerating() {return m_bAccelerating;}
	float GetAccelerationRate() {return m_nAccelerationRate;}
	float GetRotationRate() {return m_nRotationRate;}
	bool IsDecelerating() {return m_bDecelerating;}

	void SetWeight(float nW) {m_nWeight = nW;}
	void SetFriction(float nF) {m_nFriction = nF;}
	void SetCurSpeed(float nS) {m_nCurSpeed = nS;}
	void SetMaxSpeed(float nS) {m_nMaxSpeed = nS;}
	void SetAccelerating(bool bAccel) {m_bAccelerating = bAccel;}
	void SetAccelerationRate(float fA) {m_nAccelerationRate = fA;}
	void SetRotationRate(float nR) {m_nRotationRate = nR;}
	void SetDecelerating(bool bDecel) {m_bDecelerating = bDecel;}
};

#endif