#ifndef _H_CAI_
#define _H_CAI_

#include "CWayPoint.h"
#include "CKartObject.h"
#include "SGDWrappers/CSGD_Direct3D.h" // TODO: Remove when render is removed
#include "SGDWrappers/SGD_Math.h"
#include <vector>
using std::vector;

class CAi {
private:
	CKartObject*		m_pKartObject;
	vector<CWayPoint>	m_vWayPoints;
	unsigned int		m_nCurrentPoint;
	tVector2D			m_vDifference;

	void Seek(tVector2D vTargetPos);
public:
	CAi(CKartObject* pTarget = NULL); // TODO: This should not be null
	~CAi() {}
	CAi(const CAi& rCAi);
	CAi& operator=(const CAi& rCAi);

	void Update();
	void Initialize();

	void AddWayPoint(float nX, float nY);
	void AddWayPoint(CWayPoint cWayPoint);
	void AddWayPoint(tVector2D tVect);

	int GetCurrentPoint() {return m_nCurrentPoint;}
	void SetTarget(CKartObject* pTarget) {m_pKartObject = pTarget;} // TODO: Remove
	int GetNextWayPoint();
};

#endif