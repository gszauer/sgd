#include "CWayPoint.h"

CWayPoint::CWayPoint(float nX, float nY) {
	m_vCoordinates.fX = nX;
	m_vCoordinates.fY = nY;
}

CWayPoint::CWayPoint(const CWayPoint& rCWayPoint) {
	m_vCoordinates.fX = rCWayPoint.m_vCoordinates.fX;
	m_vCoordinates.fY = rCWayPoint.m_vCoordinates.fY;
}

CWayPoint& CWayPoint::operator=(const CWayPoint& rCWayPoint) {
	if (this != &rCWayPoint) {
		m_vCoordinates.fX = rCWayPoint.m_vCoordinates.fX;
		m_vCoordinates.fY = rCWayPoint.m_vCoordinates.fY;
	}
	return *this;
}