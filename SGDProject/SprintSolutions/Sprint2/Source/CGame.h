#ifndef _H_CGAME_
#define _H_CGAME_

#include "CGamePlayState.h"
#include "SGDWrappers.h"
#include "IGameState.h"
#include "CTimer.h"
#include <windows.h>
#include "CTimer.h"

class CGame {
private:
	CSGD_Direct3D*			m_pD3D;
	CSGD_TextureManager*	m_pTM;
	CSGD_DirectSound*		m_pDS;
	CSGD_WaveManager*		m_pWM;
	CSGD_DirectInput*		m_pDI;
	CSGD_MessageSystem*		m_pMS;
	CSGD_EventSystem*		m_pES;
	IGameState*				m_pCurState;
	int						m_nScreenW;
	int						m_nScreenH;
	HWND					m_hWnd;
	HINSTANCE				m_hInstance;
	CTimer*					m_pTimer;
	bool					m_bWindowed;

	bool Input();
	void Update(float fDelta);
	void Render();

	CGame();
	~CGame();
	CGame(const CGame& rCGame) {}
	CGame& operator=(const CGame& rCGame) {}
public:
	static CGame* GetInstance();
	void Initialize(HWND hWnd, HINSTANCE hInstance, int nWindowWidth, int nWindowHeight, bool bIsWindowed);
	bool Main();
	void ChangeState(IGameState *pNewState);
	void Shutdown();
	
	void SetScreenWidth(int nW) {m_nScreenW = nW;}
	void SetScreenHeight(int nH) {m_nScreenH = nH;}
	int GetScreenWidth() {return m_nScreenW;}
	int GetScreenHeight() {return m_nScreenH;}
	bool IsWindowed() {return m_bWindowed;}
};

#endif