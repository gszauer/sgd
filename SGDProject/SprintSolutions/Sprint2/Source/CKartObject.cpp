#include "CKartObject.h"
#include "CGamePlayState.h"

CKartObject::CKartObject() : CBase() {
	m_nWeight = 5.0f;
	m_nFriction = 30.0f;
	m_nAccelerationRate = 50.0;
	m_nRotationRate = 3.14f;
	m_nCurSpeed = 0.0f;
	m_nMaxSpeed = 150.0;
	m_nCurRotation = 0.0f;
	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;

	m_bAccelerating = false;
}

void CKartObject::RotateLeft() {
	m_nCurRotation -= m_nRotationRate * CTimer::GetInstance()->GetDelta();
}

void CKartObject::RotateRight() {
	m_nCurRotation += m_nRotationRate * CTimer::GetInstance()->GetDelta();
}

void CKartObject::Update(float nDelta) {
	if (IsAccelerating()) {
		if (m_nCurSpeed < m_nMaxSpeed) {
			m_nCurSpeed += m_nAccelerationRate * nDelta;
		} else if (m_nCurSpeed > m_nMaxSpeed)
			m_nCurSpeed = m_nMaxSpeed;
	} else if (IsDecelerating()) {
		if (m_nCurSpeed > 0)
			m_nCurSpeed -= m_nAccelerationRate * 2 * nDelta;
	} else {
		if (m_nCurSpeed > 0) {
			m_nCurSpeed -= m_nAccelerationRate * nDelta;
		}
	}

	m_nCurSpeed -= m_nWeight * nDelta;

	static int nBoundaries;
	static int nOffroad;
	static int nEnemieChecks;
	
	nEnemieChecks = CGamePlayState::GetInstance()->NumEnemies();
	nBoundaries = CGamePlayState::GetInstance()->GetBoundaries().size();
	nOffroad = CGamePlayState::GetInstance()->GetOffroad().size();
	
	for (int i = 0; i < nOffroad; ++i) {
		if (CGamePlayState::GetInstance()->GetOffroad()[i].CheckCollision((CBase*)this)) {
			m_nCurSpeed -= m_nFriction * nDelta;
			break;
		}
	}

	if (m_nCurSpeed < 0)
		m_nCurSpeed = 0;
	
	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;
	m_vThrust = Vector2DRotate(m_vThrust, m_nCurRotation);
	m_vThrust = m_vThrust * m_nCurSpeed;

	for (int i = 0; i < nBoundaries; ++i) {
		if (CGamePlayState::GetInstance()->GetBoundaries()[i].CheckCollision((CBase*)this)) {
			m_vThrust = m_vThrust * -5;
			break;
		}
	}
	
	SetXVelocity(m_vThrust.fX);
	SetYVelocity(m_vThrust.fY);

	CBase::Update(nDelta);
	RECT rTemp;
	CKartObject* pOponent;
	int nOffset;
	int nEnemOffset;
	for (int i = 0; i < nEnemieChecks; ++i) {
		if (&CGamePlayState::GetInstance()->GetEnemies()[i] != this)
		if (CGamePlayState::GetInstance()->GetEnemies()[i].CheckCollision((CBase*)this)) {
			
			pOponent = &CGamePlayState::GetInstance()->GetEnemies()[i];
			RECT rCollision1 = pOponent->GetCollisionRect();
			RECT rCollision2 = this->GetCollisionRect();
			IntersectRect(&rTemp, &rCollision1, &rCollision2);
			nOffset = _MAX_WEIGHT_ - this->GetWeight();
			nEnemOffset = _MAX_WEIGHT_ - pOponent->GetWeight();

			//if (rTemp.bottom - rTemp.top <= rTemp.right - rTemp.left) {
				if (rTemp.bottom >= rCollision2.bottom) {
					// This was hit from the bottom
					SetYPosition(GetYPosition() - nOffset);
					pOponent->SetYPosition(pOponent->GetYPosition() + nEnemOffset);
				} else if (rTemp.top <= rCollision2.top) {
					// This was hit from the top
					SetYPosition(GetYPosition() + nOffset);
					pOponent->SetYPosition(pOponent->GetYPosition() - nEnemOffset);
				}
			//} else {
				if (rTemp.right >= rCollision2.right) {
					// This gets hit from the right
					SetXPosition(GetXPosition() - nOffset);
					pOponent->SetXPosition(pOponent->GetXPosition() + nEnemOffset);
				} else if (rTemp.left <= rCollision2.left) {
					// This gets hit from the left
					SetXPosition(GetXPosition() + nOffset);
					pOponent->SetXPosition(pOponent->GetXPosition() - nEnemOffset);
				}
			//}

			break;
		}
	}
}

void CKartObject::Render() {
	static tVector2D vAngle;
	vAngle.fX = _ORIENTATION_X_;
	vAngle.fY = _ORIENTATION_Y_;
	vAngle = Vector2DRotate(vAngle, m_nCurRotation);
	vAngle = vAngle * 10;

	CSGD_Direct3D::GetInstance()->DrawRect(GetCollisionRect(), 255, 0, 0);
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition(), GetYPosition(), GetXPosition() + vAngle.fX, GetYPosition() + vAngle.fY, 255, 255, 255);
}

RECT CKartObject::GetCollisionRect() {
	RECT rTemp;
	SetRect(&rTemp, (GetXPosition() - GetWidth()/2), (GetYPosition() - GetHeight()/2), (GetXPosition() - GetWidth()/2) + GetWidth(), (GetYPosition() - GetHeight()/2) + GetHeight());
	return rTemp;
}