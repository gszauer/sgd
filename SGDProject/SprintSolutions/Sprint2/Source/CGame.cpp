#include "CGame.h"

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if (p) { p->Release(); p = NULL; }
#endif

CGame::CGame() {
	m_pD3D = NULL;
	m_pTM  = NULL;
	m_pDS  = NULL;
	m_pWM  = NULL;
	m_pDI  = NULL;
	m_pMS  = NULL;
	m_pES  = NULL;
	m_pCurState = NULL;
	m_nScreenW = 1;
	m_nScreenH = 1;
	m_hWnd = NULL;
	m_hInstance = NULL;
	m_pTimer = NULL;
}

CGame::~CGame() {
	Shutdown();
}

CGame* CGame::GetInstance() {
	static CGame theGame;
	return &theGame;
}

void CGame::Initialize(HWND hWnd, HINSTANCE hInstance, int nWindowWidth, int nWindowHeight, bool bIsWindowed) {
	m_pD3D	 = CSGD_Direct3D::GetInstance();
	m_pTM	 = CSGD_TextureManager::GetInstance();
	m_pDS	 = CSGD_DirectSound::GetInstance();
	m_pWM	 = CSGD_WaveManager::GetInstance();
	m_pDI	 = CSGD_DirectInput::GetInstance();
	m_pMS	 = CSGD_MessageSystem::GetInstance();
	m_pES	 = CSGD_EventSystem::GetInstance();
	m_pTimer = CTimer::GetInstance();

	m_nScreenW = nWindowWidth;
	m_nScreenH = nWindowHeight;
	m_bWindowed = bIsWindowed;

	m_pD3D->InitDirect3D(hWnd, GetScreenWidth(), GetScreenHeight(), IsWindowed(), false);
	m_pTM->InitTextureManager(m_pD3D->GetDirect3DDevice(),m_pD3D->GetSprite());
	m_pDS->InitDirectSound(hWnd);
	m_pWM->InitWaveManager(hWnd, m_pDS->GetDSObject());
	// GOTO: Input Devices
	m_pDI->InitDirectInput(hWnd,hInstance,DI_KEYBOARD);
	// GOTO: Initialize message system
	//m_pMS->InitMessageSystem(CGame::MessageProc);

	// GOTO: Set Initial State
	ChangeState(CGamePlayState::GetInstance());
}

bool CGame::Main() {
	m_pTimer->Update();
	m_pDI->ReadDevices();
	m_pWM->Update();
	
	if (m_pCurState == NULL)
		return false;
	
	if (!Input())
		return false;
	Update(m_pTimer->GetDelta());
	Render();

	return true;
}

bool CGame::Input() {
	// TODO: Check for fullscreen image
	if (m_pCurState)
		return m_pCurState->Input();
	return true;
}

void CGame::Update(float fDelta) {
	if (m_pCurState)
		m_pCurState->Update(fDelta);
}

void CGame::Render() {
	m_pD3D->Clear(0, 0, 255);
	m_pD3D->DeviceBegin();
	m_pD3D->SpriteBegin();

	if (m_pCurState)
		m_pCurState->Render();

	m_pD3D->SpriteEnd();
	m_pD3D->DeviceEnd();
	m_pD3D->Present();
}

void CGame::ChangeState(IGameState *pNewState) {
	if (m_pCurState)
		m_pCurState->Exit();

	m_pCurState = pNewState;

	if (m_pCurState)
		m_pCurState->Enter();
}

void CGame::Shutdown() {
	ChangeState(NULL);

	if (m_pES)
		m_pES->ShutdownEventSystem();
	if (m_pMS)
		m_pMS->ShutdownMessageSystem();
	if (m_pDI)
		m_pDI->ShutdownDirectInput();
	if (m_pWM)
		m_pWM->ShutdownWaveManager();
	if (m_pDS)
		m_pDS->ShutdownDirectSound();
	if (m_pTM)
		m_pTM->ShutdownTextureManager();
	if (m_pD3D)
		m_pD3D->ShutdownDirect3D();

	m_pD3D = NULL;
	m_pTM  = NULL;
	m_pDS  = NULL;
	m_pWM  = NULL;
	m_pDI  = NULL;
	m_pMS  = NULL;
	m_pES  = NULL;
}
