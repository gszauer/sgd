////////////////////////////////////////////////
//	File	:	"CSGD_ObjectManager.cpp"
//
//	Author	:	David Brown (DB)
//
//	Purpose	:	To contain and manage all of our game objects.
/////////////////////////////////////////////////
#include "CSGD_ObjectManager.h"
#include "../CBaseObject.h"

//	Macro to safely delete pointers.
#ifndef SAFE_DELETE
#define SAFE_DELETE(p)			if (p) { delete p; p = NULL; }
#endif

CSGD_ObjectManager::CSGD_ObjectManager(void)
{
}

CSGD_ObjectManager::~CSGD_ObjectManager(void)
{
}

void CSGD_ObjectManager::UpdateObjects(float fElapsedTime)
{
	vector<CBase*>::iterator iter = m_vObjectList.begin();
	vector<CBase*> DeadObjects;

	while(iter != m_vObjectList.end())
	{

		if ((*iter)->IsActive())
		{
			(*iter)->Update(fElapsedTime);
		}
		else
		{
			//DeadObjects.push_back(*iter);
		}
		iter++;
	}
	/*for (unsigned int i = 0; i <  DeadObjects.size(); i++)
	{
		RemoveObject(DeadObjects[i]);
	}*/

}

void CSGD_ObjectManager::RenderObjects(void)
{
	for (unsigned int i=0; i < m_vObjectList.size(); i++)
		m_vObjectList[i]->Render();
}

void CSGD_ObjectManager::AddObject(CBase* pObject)
{
	//	Check for valid object pointer
	if (pObject == NULL)
		return;

	//	Add object to object list
	m_vObjectList.push_back(pObject);

	//	Add my reference to it.
	pObject->AddRef();
}

void CSGD_ObjectManager::RemoveObject(CBase* pObject)
{
	//	Check for valid object pointer
	if (pObject == NULL)
		return;

	for (vector<CBase*>::iterator iter = m_vObjectList.begin();
		iter != m_vObjectList.end();
		iter++)
	{
		// if this is the object we are looking for.
		if ((*iter) == pObject)
		{
			// Remove my reference to this object.
			(*iter)->Release();

			// Remove the object from the list
			iter = m_vObjectList.erase(iter);
			break;
		}
	}
}

void CSGD_ObjectManager::RemoveAllObjects(void)
{
	//	Call Release() on all objects.
	for (unsigned int i=0; i < m_vObjectList.size(); i++)
	{
		m_vObjectList[i]->Release();
	}

	//	Collapse the vector
	m_vObjectList.clear();
}


CSGD_ObjectManager* CSGD_ObjectManager::GetInstance()
{
	static CSGD_ObjectManager theObjectManager;
	return &theObjectManager;

}
void CSGD_ObjectManager::DeleteInstance()
{
	if (CSGD_ObjectManager::GetInstance()!= NULL)
	{
		delete CSGD_ObjectManager::GetInstance();

	}
}
void CSGD_ObjectManager::CheckCollision()
{
	for (unsigned int i = 0 ; i < m_vObjectList.size() ; i++)
	{
		for (unsigned int j = 0; j < m_vObjectList.size() ; j++)
		{

			if (m_vObjectList[i]->GetType() == m_vObjectList[j]->GetType())
			{
				continue;
			}
			if (m_vObjectList[i]->CheckCollision(m_vObjectList[j]))
			{
				break;
			}

		}
	}

}