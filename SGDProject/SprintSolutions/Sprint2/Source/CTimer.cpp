#include "CTimer.h"

CTimer::CTimer() {
	m_nFrameCount = 0;
	m_nFPS = 0;
	m_dwFPSTimeStamp = GetTickCount();
	m_dwPrevTimeStamp = GetTickCount();
	m_nElapsedTime = 0.0f;
	m_nGameTime = 0.0f;
}

CTimer* CTimer::GetInstance() {
	static CTimer gameTimer;
	return &gameTimer;
}

void CTimer::Initialize() {
	m_nFrameCount = 0;
	m_nFPS = 0;
	m_dwFPSTimeStamp = GetTickCount();
	m_dwPrevTimeStamp = GetTickCount();
	m_nElapsedTime = 0.0f;
	m_nGameTime = 0.0f;
}

void CTimer::Update() {
	m_dwStartTimeStamp = GetTickCount();
	m_nElapsedTime = (float)(m_dwStartTimeStamp - m_dwPrevTimeStamp) / 1000.0f; // 10.0f = 1 second
	m_dwPrevTimeStamp = m_dwStartTimeStamp;

	m_nFrameCount++;
	if (GetTickCount() - m_dwFPSTimeStamp >= 1000) {
		m_nFPS = m_nFrameCount;
		m_nFrameCount = 0;
		m_dwFPSTimeStamp = GetTickCount();
	}

	m_nGameTime += m_nElapsedTime;
}

float CTimer::GetGameTime() {
	return m_nGameTime;
}

float CTimer::GetDelta() {
	return m_nElapsedTime;
}

float CTimer::GetFps() {
	return m_nFPS;
}