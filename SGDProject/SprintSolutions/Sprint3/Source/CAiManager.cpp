#include "CAiManager.h"

CAiManager* CAiManager::GetInstance() {
	static CAiManager masterBrain;
	return &masterBrain;
}

int CAiManager::AddBrain(CKartObject* pTarget) {
	CAi* temp = new CAi(pTarget);
	m_vAi.push_back(temp);
	return m_vAi.size() - 1;
}

void CAiManager::NoBrainz() {
	static int nSize;
	nSize = m_vAi.size();
	for (int i(0); i < nSize; ++i) {
		delete m_vAi[i];
	}
	m_vAi.clear();
}

CAi* CAiManager::Brain(int nIndex) {
	return m_vAi[nIndex];
}

void CAiManager::Think() {
	static int nSize;
	nSize = m_vAi.size();
	for (int i(0); i < nSize; ++i) {
		m_vAi[i]->Update();
	}
}

void CAiManager::Shutdown() {
	NoBrainz();
}