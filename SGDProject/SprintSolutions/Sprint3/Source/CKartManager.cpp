#include "CKartManager.h"

CKartManager* CKartManager::GetInstance() {
	static CKartManager DasManager;
	return &DasManager;
}

struct compareKartDistances {
	bool operator ()(CKartObject *lhs, CKartObject *rhs) { 
		return lhs->GetDistance() < rhs->GetDistance(); 
	}
};

void CKartManager::Sort() {
	std::sort(m_vKarts.begin(), m_vKarts.end(), compareKartDistances());
}

void CKartManager::AddKart(CKartObject* pKart) {
	m_vKarts.push_back(pKart);
}

void CKartManager::ClearKarts() {
	m_vKarts.clear();
}

CKartObject* CKartManager::GetNextKart(CKartObject* pOrigin) {
	Sort();
	static int nIndex;
	static int nSize;
	nSize = m_vKarts.size();
	
	for (int i(0); i < nSize; ++i) {
		if (m_vKarts[i] == pOrigin) {
			nIndex = i;
			break;
		}
	}
	
	if (++nIndex >= nSize)
		return pOrigin;
	else
		return m_vKarts[nIndex];
}

CKartObject* CKartManager::GetFirstKart() {
	Sort();
	return m_vKarts[m_vKarts.size() - 1];
}