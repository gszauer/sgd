#ifndef _H_IGAMESTATE_
#define _H_IGAMESTATE_

class IGameState {
public:
	virtual void Enter(void) = 0;
	virtual bool Input(void) = 0;
	virtual void Update(float fDelta) = 0;
	virtual void Render(void) = 0;
	virtual void Exit(void) = 0;
	virtual ~IGameState() = 0 {};
};

#endif