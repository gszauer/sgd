#include "CGAmePlayState.h"

CGamePlayState::CGamePlayState() {
	CBoundaryObject	boundaries[4];
	COffroadObject	offroad[17];
	m_vTrackCenterPoint.fX = 320;
	m_vTrackCenterPoint.fY = 240;
	SetRect(&m_rStartLine, 72, 162, 144, 163);

	m_pPlayer = (CKartObject*)CSGD_ObjectFactory<string, CBase>::GetInstance()->CreateObject("CKartObject"); 
	m_pPlayer->SetXPosition(90);
	m_pPlayer->SetYPosition(288);
	m_pPlayer->SetWidth(18);
	m_pPlayer->SetHeight(18);
	m_pPlayer->SetWeight(4);
	CSGD_ObjectManager::GetInstance()->AddObject(m_pPlayer);
	CKartManager::GetInstance()->AddKart(m_pPlayer);

	CKartObject* pEnemy;
	for (int i = 0; i < _NUM_ENEMIES_; ++i) {
		pEnemy = (CKartObject*)CSGD_ObjectFactory<string, CBase>::GetInstance()->CreateObject("CKartObject"); 
		pEnemy->SetWidth(18);
		pEnemy->SetHeight(18);
		if (i % 2 == 0)
			pEnemy->SetXPosition(72 + pEnemy->GetWidth() / 2);
		else
			pEnemy->SetXPosition(108 + pEnemy->GetWidth() / 2);
		pEnemy->SetYPosition(252 - i * 22);
		pEnemy->SetAccelerating(true);
		pEnemy->SetWeight(3);
		pEnemy->SetMaxSpeed(130);
		CSGD_ObjectManager::GetInstance()->AddObject(pEnemy);
		CKartManager::GetInstance()->AddKart(pEnemy);
		CAiManager::GetInstance()->AddBrain(pEnemy);
		//CAiManager::GetInstance()->Brain(nAi)->SetTarget(pEnemy);
		pEnemy->Release();
	}

	for (int i = 0; i < _NUM_ENEMIES_; ++i) {
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(99, 189);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(108, 144);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(153, 117);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(315, 117);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(486, 117);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(522, 135);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(549, 171);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(549, 243);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(549, 324);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(522, 369);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(477, 387);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(333, 387);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(153, 387);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(117, 369);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(99, 333);
		CAiManager::GetInstance()->Brain(i)->AddWayPoint(99, 261);
	}

	m_ucAccelerate = DIK_W;
	m_ucDecelerate = DIK_S;
	m_ucLeft = DIK_A;
	m_ucRight = DIK_D;
	m_ucItem = DIK_SPACE;

	// Set up stage boundaries
	boundaries[0].SetXPosition(0);
	boundaries[0].SetYPosition(0);
	boundaries[0].SetWidth(640);
	boundaries[0].SetHeight(18);

	boundaries[1].SetXPosition(0);
	boundaries[1].SetYPosition(450);
	boundaries[1].SetWidth(640);
	boundaries[1].SetHeight(30);

	boundaries[2].SetXPosition(0);
	boundaries[2].SetYPosition(18);
	boundaries[2].SetWidth(18);
	boundaries[2].SetHeight(432);

	boundaries[3].SetXPosition(612);
	boundaries[3].SetYPosition(18);
	boundaries[3].SetWidth(28);
	boundaries[3].SetHeight(432);

	// Set up offroad objects
	offroad[0].SetXPosition(18);
	offroad[0].SetYPosition(18);
	offroad[0].SetWidth(54);
	offroad[0].SetHeight(432);

	offroad[1].SetXPosition(576);
	offroad[1].SetYPosition(18);
	offroad[1].SetWidth(36);
	offroad[1].SetHeight(432);

	offroad[2].SetXPosition(72);
	offroad[2].SetYPosition(18);
	offroad[2].SetWidth(504);
	offroad[2].SetHeight(72);

	offroad[3].SetXPosition(72);
	offroad[3].SetYPosition(414);
	offroad[3].SetWidth(504);
	offroad[3].SetHeight(36);

	offroad[4].SetXPosition(72);
	offroad[4].SetYPosition(90);
	offroad[4].SetWidth(36);
	offroad[4].SetHeight(18);

	offroad[5].SetXPosition(540);
	offroad[5].SetYPosition(90);
	offroad[5].SetWidth(36);
	offroad[5].SetHeight(18);

	offroad[6].SetXPosition(72);
	offroad[6].SetYPosition(396);
	offroad[6].SetWidth(36);
	offroad[6].SetHeight(18);

	offroad[7].SetXPosition(540);
	offroad[7].SetYPosition(396);
	offroad[7].SetWidth(36);
	offroad[7].SetHeight(18);

	offroad[8].SetXPosition(72);
	offroad[8].SetYPosition(108);
	offroad[8].SetWidth(18);
	offroad[8].SetHeight(18);

	offroad[9].SetXPosition(558);
	offroad[9].SetYPosition(108);
	offroad[9].SetWidth(18);
	offroad[9].SetHeight(18);

	offroad[10].SetXPosition(72);
	offroad[10].SetYPosition(378);
	offroad[10].SetWidth(18);
	offroad[10].SetHeight(18);

	offroad[11].SetXPosition(558);
	offroad[11].SetYPosition(378);
	offroad[11].SetWidth(18);
	offroad[11].SetHeight(18);

	offroad[12].SetXPosition(144);
	offroad[12].SetYPosition(162);
	offroad[12].SetWidth(360);
	offroad[12].SetHeight(180);

	offroad[13].SetXPosition(162);
	offroad[13].SetYPosition(144);
	offroad[13].SetWidth(324);
	offroad[13].SetHeight(18);

	offroad[14].SetXPosition(126);
	offroad[14].SetYPosition(180);
	offroad[14].SetWidth(18);
	offroad[14].SetHeight(144);

	offroad[15].SetXPosition(162);
	offroad[15].SetYPosition(342);
	offroad[15].SetWidth(324);
	offroad[15].SetHeight(18);

	offroad[16].SetXPosition(504);
	offroad[16].SetYPosition(180);
	offroad[16].SetWidth(18);
	offroad[16].SetHeight(144);

	for (int i = 0; i < 4; ++i) {
		m_vBoundarys.push_back(boundaries[i]);
	}

	for (int i  = 0; i < 17; ++i) {
		m_vOffroad.push_back(offroad[i]);
	}
}

vector<CBoundaryObject>& CGamePlayState::GetBoundaries() {
	return m_vBoundarys;
}

vector<COffroadObject>& CGamePlayState::GetOffroad() {
	return m_vOffroad;
}

CKartObject* CGamePlayState::GetNextKart() {
	return NULL;
}

CGamePlayState* CGamePlayState::GetInstance() {
 static CGamePlayState gamePlayState;
 return &gamePlayState;
}

void CGamePlayState::Reset() {

}

void CGamePlayState::Enter() {
	int r = CGame::GetInstance()->GetScreenWidth();
	int b = CGame::GetInstance()->GetScreenHeight();
	SetRect(&rScreen, 0, 0, r, b);
}

bool CGamePlayState::Input() {
	m_pPlayer->SetAccelerating(CSGD_DirectInput::GetInstance()->KeyDown(m_ucAccelerate));
	m_pPlayer->SetDecelerating(CSGD_DirectInput::GetInstance()->KeyDown(m_ucDecelerate));

	if (CSGD_DirectInput::GetInstance()->KeyPressed(m_ucItem)) {
		switch(m_pPlayer->GetItem()) {
			case ITEM_GREENSHELL:
					CSGD_MessageSystem::GetInstance()->SendMsg(new CreateGreenShellMessage(m_pPlayer));
				break;
			case ITEM_REDSHELL:
					CSGD_MessageSystem::GetInstance()->SendMsg(new CreateRedShellMessage(m_pPlayer));
				break;
			case ITEM_BLUESHELL:
					CSGD_MessageSystem::GetInstance()->SendMsg(new CreateBlueShellMessage(m_pPlayer));
				break;
		}
		m_pPlayer->SetItem(ITEM_MAX);
	}

	if (CSGD_DirectInput::GetInstance()->KeyPressed(DIK_1)) {
		m_pPlayer->SetItem(ITEM_GREENSHELL);
	}

	if (CSGD_DirectInput::GetInstance()->KeyPressed(DIK_2)) {
		m_pPlayer->SetItem(ITEM_REDSHELL);
	}

	if (CSGD_DirectInput::GetInstance()->KeyPressed(DIK_3)) {
		m_pPlayer->SetItem(ITEM_BLUESHELL);
	}


	if (CSGD_DirectInput::GetInstance()->KeyDown(m_ucLeft)) {
		m_pPlayer->RotateLeft();
		return true;
	} else if (CSGD_DirectInput::GetInstance()->KeyDown(m_ucRight)) {
		m_pPlayer->RotateRight();
		return true;
	}

	return true;
}

void CGamePlayState::Update(float fDelta) {	
	//CAiManager::GetInstance()->Think();
	//CSGD_ObjectManager::GetInstance()->UpdateObjects(fDelta);
	CSGD_ObjectManager::GetInstance()->CheckCollision();
}

void CGamePlayState::Render() {
	CSGD_Direct3D::GetInstance()->DrawRect(rScreen, 0, 255, 255);
	for (int i = 0; i < 4; ++i)
		m_vBoundarys[i].Render();
	for (int i = 0; i < 17; ++i)
		m_vOffroad[i].Render();
	CSGD_Direct3D::GetInstance()->DrawRect(m_rStartLine, 255, 0, 255);

	CSGD_ObjectManager::GetInstance()->RenderObjects();
	static RECT rCenter = {m_vTrackCenterPoint.fX - 5, m_vTrackCenterPoint.fY - 5, m_vTrackCenterPoint.fX + 5, m_vTrackCenterPoint.fY + 5};
	CSGD_Direct3D::GetInstance()->DrawRect(rCenter, 0, 0, 0);
	
	// Debug info
	CSGD_Direct3D::GetInstance()->SpriteEnd();
	char buffer3[255];
	sprintf(buffer3, "FPS: %.0f", CTimer::GetInstance()->GetFps());
	char buffer4[255];
	sprintf(buffer4, "Laps: %d", m_pPlayer->GetLaps());
	CSGD_Direct3D::GetInstance()->DrawText(buffer3, 0, 0);
	CSGD_Direct3D::GetInstance()->SpriteBegin();
}

void CGamePlayState::Exit() {

}