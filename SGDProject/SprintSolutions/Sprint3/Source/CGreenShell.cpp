#include "CGreenShell.h"

void CGreenShell::Update(float nDelta) {

	static int nBoundaries;
	nBoundaries = CGamePlayState::GetInstance()->GetBoundaries().size();

	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;
	m_vThrust = Vector2DRotate(m_vThrust, m_nAngle);
	m_vThrust = m_vThrust * m_nSpeed;

	for (int i = 0; i < nBoundaries; ++i) {
		if (CGamePlayState::GetInstance()->GetBoundaries()[i].CheckCollision((CBase*)this)) {
			CSGD_MessageSystem::GetInstance()->SendMsg(new CDestroyGreenShell(this));
			break;
		}
	}

	SetXVelocity(m_vThrust.fX);
	SetYVelocity(m_vThrust.fY);

	CBase::Update(nDelta);
}

void CGreenShell::Render() {
	CSGD_Direct3D::GetInstance()->LineBegin();
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition() - 5, GetYPosition() - 5, GetXPosition() + 5, GetYPosition() + 5, 0, 255, 0);
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition() + 5, GetYPosition() - 5, GetXPosition() - 5, GetYPosition() + 5, 0, 255, 0);
	CSGD_Direct3D::GetInstance()->LineEnd();
}