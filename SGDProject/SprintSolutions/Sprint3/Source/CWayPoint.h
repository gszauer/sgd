#ifndef _H_CWAYPOINT_
#define _H_CWAYPOINT_

#include "SGDWrappers/SGD_Math.h"

class CWayPoint {
private:
	tVector2D m_vCoordinates;
public:
	CWayPoint(float nX = 0.0f, float nY = 0.0f);
	~CWayPoint() {}
	CWayPoint(const CWayPoint& rCWayPoint);
	CWayPoint& operator=(const CWayPoint& rCWayPoint);

	float GetXPosition() {return m_vCoordinates.fX;}
	float GetYPosition() {return m_vCoordinates.fY;}
	tVector2D GetPositionVector() {return m_vCoordinates;}

	void SetXPosition(float nX) {m_vCoordinates.fX = nX;}
	void SetYPosition(float nY) {m_vCoordinates.fY = nY;}
	void SetPositionVector(tVector2D vect) {m_vCoordinates = vect;}
};

#endif