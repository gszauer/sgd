#include "CBlueShell.h"

void CBlueShell::Update(float nDelta) {
	m_pTargetObject = CKartManager::GetInstance()->GetFirstKart();
	static int nBoundaries;
	nBoundaries = CGamePlayState::GetInstance()->GetBoundaries().size();

	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;
	static tVector2D tUpVector = {0, -1};
	m_vDifference = m_pTargetObject->GetPositionVector() - GetPositionVector();
	m_nAngle = AngleBetweenVectors(tUpVector, m_vDifference);
	if (GetXPosition() > m_pTargetObject->GetXPosition())
		m_nAngle *= -1;
	m_vThrust = Vector2DRotate(m_vThrust, m_nAngle);
	m_vThrust = m_vThrust * m_nSpeed;

	for (int i = 0; i < nBoundaries; ++i) {
		if (CGamePlayState::GetInstance()->GetBoundaries()[i].CheckCollision((CBase*)this)) {
			CSGD_MessageSystem::GetInstance()->SendMsg(new CDestroyBlueShell(this));
			break;
		}
	}

	SetXVelocity(m_vThrust.fX);
	SetYVelocity(m_vThrust.fY);

	CBase::Update(nDelta);
}

void CBlueShell::Render() {
	CSGD_Direct3D::GetInstance()->LineBegin();
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition() - 5, GetYPosition() - 5, GetXPosition() + 5, GetYPosition() + 5, 0, 0, 255);
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition() + 5, GetYPosition() - 5, GetXPosition() - 5, GetYPosition() + 5, 0, 0, 255);
	CSGD_Direct3D::GetInstance()->LineEnd();
}