#include "CAi.h"

CAi::CAi(CKartObject* pTarget) {
	m_pKartObject = pTarget;
	m_vDifference.fX = 0;
	m_vDifference.fY = 0;
}

CAi::CAi(const CAi& rCAi) {
	m_pKartObject = rCAi.m_pKartObject;
	m_vWayPoints = rCAi.m_vWayPoints;
}

CAi& CAi::operator=(const CAi& rCAi) {
	if (this != &rCAi) {
		m_pKartObject = rCAi.m_pKartObject;
		m_vWayPoints = rCAi.m_vWayPoints;
	}
	return *this;
}

void CAi::Update() {
	if (Vector2DLength(m_vDifference) < 10.0f)
		GetNextWayPoint();
	Seek(m_vWayPoints[m_nCurrentPoint].GetPositionVector());
}

int CAi::GetNextWayPoint() {
	if (++m_nCurrentPoint >= m_vWayPoints.size()) {
		m_nCurrentPoint = 0; 
	}
	return m_nCurrentPoint;
}

void CAi::Seek(tVector2D vTargetPos) {	
	m_vDifference = vTargetPos - m_pKartObject->GetPositionVector();
	static tVector2D tUpVector = {0, -1};
	static float nAngle;
	nAngle = AngleBetweenVectors(tUpVector, m_vDifference);
	if (m_pKartObject->GetXPosition() > vTargetPos.fX)
		nAngle *= -1;
	m_pKartObject->SetRotationAngle(nAngle);

}

void CAi::Initialize() {
	m_pKartObject->SetXPosition(m_vWayPoints[0].GetXPosition());
	m_pKartObject->SetYPosition(m_vWayPoints[0].GetYPosition());
}

void CAi::AddWayPoint(float nX, float nY) {
	CWayPoint wp(nX, nY);
	m_vWayPoints.push_back(wp);
}

void CAi::AddWayPoint(CWayPoint cWayPoint) {
	CWayPoint wp = cWayPoint;
	m_vWayPoints.push_back(wp);
}

void CAi::AddWayPoint(tVector2D tVect) {
	CWayPoint wp(tVect.fX, tVect.fY);
	m_vWayPoints.push_back(wp);
}