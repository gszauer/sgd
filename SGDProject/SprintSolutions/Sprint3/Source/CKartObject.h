#ifndef _H_CKARTOBJECT_
#define _H_CKARTOBJECT_

#include "CBaseObject.h"
#include "CTimer.h"

class CKartObject : public CBase, public IListener {
private:
	tVector2D	m_vThrust; // Rotation + speed = thrust
	float		m_nWeight;
	float		m_nFriction;
	float		m_nAccelerationRate;
	float		m_nRotationRate;
	float		m_nCurRotation;
	float		m_nCurSpeed;
	float		m_nMaxSpeed;
	DWORD		m_dwSpinoutStart;
	DWORD		m_dwFinishCross;
	bool		m_bAccelerating;
	bool		m_bDecelerating;
	bool		m_bSpinningOut;
	int			m_nItem;

	tVector2D		m_tDifference;
	//tVector2D		m_tOldDifference;
	unsigned int	m_nLaps;
	float			m_nAngle;
	float			m_nOldAngle;
	float			m_nTotalAngle;
	bool			m_bCountingAngles;
public:
	CKartObject();
	~CKartObject() {}
	void Render();
	RECT GetCollisionRect();
	void HandleEvent(CEvent* pEvent);

	void RotateLeft();
	void RotateRight();
	void Update(float nDelta);
	bool CheckCollision(CBase* pBase);
	void Spinout();

	float GetDistance() {return m_nTotalAngle;} // TODO: Change
	bool IsCounting() {return m_bCountingAngles;}
	void StartCounting() {m_bCountingAngles = true;}
	void StopCounting() {m_bCountingAngles = false;}
	void IncreaseLap();
	
	float GetWeight() {return m_nWeight;}
	float GetFriction() {return m_nFriction;}
	float GetCurSpeed() {return m_nCurSpeed;}
	float GetMaxSpeed() {return m_nMaxSpeed;}
	bool CanAccelerate() {return (m_nCurSpeed < m_nMaxSpeed);}
	bool IsAccelerating() {return m_bAccelerating;}
	float GetAccelerationRate() {return m_nAccelerationRate;}
	float GetRotationRate() {return m_nRotationRate;}
	bool IsDecelerating() {return m_bDecelerating;}
	float GetRotationAngle() {return m_nCurRotation;}
	tVector2D GetThrustVector() {return m_vThrust;}
	int GetItem() {return m_nItem;}
	unsigned int GetLaps() {return m_nLaps;}

	void SetWeight(float nW) {m_nWeight = nW;}
	void SetFriction(float nF) {m_nFriction = nF;}
	void SetCurSpeed(float nS) {m_nCurSpeed = nS;}
	void SetMaxSpeed(float nS) {m_nMaxSpeed = nS;}
	void SetAccelerating(bool bAccel) {m_bAccelerating = bAccel;}
	void SetAccelerationRate(float fA) {m_nAccelerationRate = fA;}
	void SetRotationRate(float nR) {m_nRotationRate = nR;}
	void SetDecelerating(bool bDecel) {m_bDecelerating = bDecel;}
	void SetRotationAngle(float nAngle) {m_nCurRotation = nAngle;}
	void SetItem(int nItem) {m_nItem = nItem;}
};

#endif