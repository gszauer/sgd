#ifndef _H_KARTMANAGER_
#define _H_KARTMANAGER_

#include "CKartObject.h"
#include <vector>
using std::vector;

class CKartManager {
private:
	vector<CKartObject*>	m_vKarts;

	CKartManager() {m_vKarts.reserve(10);}
	~CKartManager() {ClearKarts();}
	CKartManager(const CKartManager& rCKartManager) {}
	CKartManager& operator=(const CKartManager& rCKartManager) {}
	bool ObjectCompare(const CKartObject& rKart1, const CKartObject& rKart2);
public:
	static CKartManager* GetInstance();
	void AddKart(CKartObject* pKart);
	void Sort();
	void ClearKarts();
	CKartObject* GetNextKart(CKartObject* pOrigin);
	CKartObject* GetFirstKart();
};

#endif