#include "CGame.h"

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if (p) { p->Release(); p = NULL; }
#endif

CGame::CGame() {
	m_pD3D = NULL;
	m_pTM  = NULL;
	m_pDS  = NULL;
	m_pWM  = NULL;
	m_pDI  = NULL;
	m_pMS  = NULL;
	m_pES  = NULL;
	m_pCurState = NULL;
	m_nScreenW = 1;
	m_nScreenH = 1;
	m_hWnd = NULL;
	m_hInstance = NULL;
	m_pTimer = NULL;
	m_pOM = NULL;
	m_pOF = NULL;

}

CGame::~CGame() {
	Shutdown();
}

CGame* CGame::GetInstance() {
	static CGame theGame;
	return &theGame;
}

void CGame::Initialize(HWND hWnd, HINSTANCE hInstance, int nWindowWidth, int nWindowHeight, bool bIsWindowed) {
	m_pD3D	 = CSGD_Direct3D::GetInstance();
	m_pTM	 = CSGD_TextureManager::GetInstance();
	m_pDS	 = CSGD_DirectSound::GetInstance();
	m_pWM	 = CSGD_WaveManager::GetInstance();
	m_pDI	 = CSGD_DirectInput::GetInstance();
	m_pMS	 = CSGD_MessageSystem::GetInstance();
	m_pES	 = CSGD_EventSystem::GetInstance();
	m_pTimer = CTimer::GetInstance();
	m_pOM	 = CSGD_ObjectManager::GetInstance();
	m_pOF	 = CSGD_ObjectFactory<string, CBase>::GetInstance();

	m_nScreenW = nWindowWidth;
	m_nScreenH = nWindowHeight;
	m_bWindowed = bIsWindowed;

	m_pD3D->InitDirect3D(hWnd, GetScreenWidth(), GetScreenHeight(), IsWindowed(), false);
	m_pTM->InitTextureManager(m_pD3D->GetDirect3DDevice(),m_pD3D->GetSprite());
	m_pDS->InitDirectSound(hWnd);
	m_pWM->InitWaveManager(hWnd, m_pDS->GetDSObject());
	// GOTO: Input Devices
	m_pDI->InitDirectInput(hWnd,hInstance,DI_KEYBOARD);
	m_pMS->InitMessageSystem(CGame::MessageProc);

	m_pOF->RegisterClassType<CBase>("CBase");
	m_pOF->RegisterClassType<CKartObject>("CKartObject");
	m_pOF->RegisterClassType<CGreenShell>("CGreenShell");
	m_pOF->RegisterClassType<CRedShell>("CRedShell");
	m_pOF->RegisterClassType<CBlueShell>("CBlueShell");

	// GOTO: Set Initial State
	ChangeState(CGamePlayState::GetInstance());
}

bool CGame::Main() {
	m_pTimer->Update();
	m_pDI->ReadDevices();
	m_pWM->Update();
	
	if (m_pCurState == NULL)
		return false;
	
	if (!Input())
		return false;
	Update(m_pTimer->GetDelta());
	Render();

	return true;
}

bool CGame::Input() {
	// TODO: Check for fullscreen image
	if (m_pCurState)
		return m_pCurState->Input();
	return true;
}

void CGame::Update(float fDelta) {
	if (m_pCurState)
		m_pCurState->Update(fDelta);
	
	m_pES->ProcessEvents();
	m_pMS->ProcessMessages();
}

void CGame::Render() {
	m_pD3D->Clear(0, 0, 255);
	m_pD3D->DeviceBegin();
	m_pD3D->SpriteBegin();

	if (m_pCurState)
		m_pCurState->Render();

	m_pD3D->SpriteEnd();
	m_pD3D->DeviceEnd();
	m_pD3D->Present();
}

void CGame::ChangeState(IGameState *pNewState) {
	if (m_pCurState)
		m_pCurState->Exit();

	m_pCurState = pNewState;

	if (m_pCurState)
		m_pCurState->Enter();
}

void CGame::Shutdown() {
	ChangeState(NULL);

	if (m_pES)
		m_pES->ShutdownEventSystem();
	if (m_pMS)
		m_pMS->ShutdownMessageSystem();
	if (m_pDI)
		m_pDI->ShutdownDirectInput();
	if (m_pWM)
		m_pWM->ShutdownWaveManager();
	if (m_pDS)
		m_pDS->ShutdownDirectSound();
	if (m_pTM)
		m_pTM->ShutdownTextureManager();
	if (m_pD3D)
		m_pD3D->ShutdownDirect3D();

	m_pD3D = NULL;
	m_pTM  = NULL;
	m_pDS  = NULL;
	m_pWM  = NULL;
	m_pDI  = NULL;
	m_pMS  = NULL;
	m_pES  = NULL;
}

void CGame::MessageProc(CBaseMessage* pMsg) {
	switch (pMsg->GetMsgID()) {
		case MSG_CREATEGREENSHELL:
			{
				CGreenShell* pGreenShell = (CGreenShell*) CSGD_ObjectFactory<string, CBase>::GetInstance()->CreateObject("CGreenShell");
				CreateGreenShellMessage* pMessage = (CreateGreenShellMessage*)pMsg;
				pGreenShell->SetXPosition(pMessage->GetXPosition());
				pGreenShell->SetYPosition(pMessage->GetYPosition());
				pGreenShell->SetCurrentSpeed(pMessage->GetCurrentSpeed());
				pGreenShell->SetXVelocity(pMessage->GetXVelocity());
				pGreenShell->SetYVelocity(pMessage->GetYVelocity());
				pGreenShell->SetCurrentAngle(pMessage->GetCurAngle());
				pGreenShell->SetWidth(pMessage->GetWidth());
				pGreenShell->SetHeight(pMessage->GetHeight());
				pGreenShell->SetOriginKart(pMessage->GetOrigin());
				CSGD_ObjectManager::GetInstance()->AddObject((CBase*)pGreenShell);
				pGreenShell->Release();
			}
			break;
		case MSG_DESTROYGREENSHELL:
			{
				CGame* pGame = CGame::GetInstance();
				CDestroyGreenShell* pDestroy = (CDestroyGreenShell*)pMsg;
				pGame->m_pOM->RemoveObject(pDestroy->GetTarget());
			}
			break;
		case MSG_KARTSPINOUT:
			{
				CGame* pGame = CGame::GetInstance();
				CKartSpinoutMessage* pSpinout = (CKartSpinoutMessage*)pMsg;
				pSpinout->GetTarget()->Spinout();
			}
			break;
		
		case MSG_CREATEREDSHELL:
			{
				CRedShell* pRedShell = (CRedShell*) CSGD_ObjectFactory<string, CBase>::GetInstance()->CreateObject("CRedShell");
				CreateRedShellMessage* pMessage = (CreateRedShellMessage*)pMsg;
				pRedShell->SetXPosition(pMessage->GetXPosition());
				pRedShell->SetYPosition(pMessage->GetYPosition());
				pRedShell->SetCurrentSpeed(pMessage->GetCurrentSpeed());
				pRedShell->SetXVelocity(pMessage->GetXVelocity());
				pRedShell->SetYVelocity(pMessage->GetYVelocity());
				pRedShell->SetCurrentAngle(pMessage->GetCurAngle());
				pRedShell->SetWidth(pMessage->GetWidth());
				pRedShell->SetHeight(pMessage->GetHeight());
				pRedShell->SetOriginKart(pMessage->GetOrigin());
				CSGD_ObjectManager::GetInstance()->AddObject((CBase*)pRedShell);
				pRedShell->Release();
			}
			break;
		case MSG_DESTROYREDSHELL:
			{
				CGame* pGame = CGame::GetInstance();
				CDestroyRedShell* pDestroy = (CDestroyRedShell*)pMsg;
				pGame->m_pOM->RemoveObject(pDestroy->GetTarget());
			}
			break;
		case MSG_CREATEBLUESHELL:
			{
				CBlueShell* pBlueShell = (CBlueShell*) CSGD_ObjectFactory<string, CBase>::GetInstance()->CreateObject("CBlueShell");
				CreateBlueShellMessage* pMessage = (CreateBlueShellMessage*)pMsg;
				pBlueShell->SetXPosition(pMessage->GetXPosition());
				pBlueShell->SetYPosition(pMessage->GetYPosition());
				pBlueShell->SetCurrentSpeed(pMessage->GetCurrentSpeed());
				pBlueShell->SetXVelocity(pMessage->GetXVelocity());
				pBlueShell->SetYVelocity(pMessage->GetYVelocity());
				pBlueShell->SetCurrentAngle(pMessage->GetCurAngle());
				pBlueShell->SetWidth(pMessage->GetWidth());
				pBlueShell->SetHeight(pMessage->GetHeight());
				pBlueShell->SetOriginKart(pMessage->GetOrigin());
				pBlueShell->SetTargetObject(pMessage->GetTarget());
				CSGD_ObjectManager::GetInstance()->AddObject((CBase*)pBlueShell);
				pBlueShell->Release();
			}
			break;
		case MSG_DESTROYBLUESHELL:
			{
				CGame* pGame = CGame::GetInstance();
				CDestroyBlueShell* pDestroy = (CDestroyBlueShell*)pMsg;
				pGame->m_pOM->RemoveObject(pDestroy->GetTarget());
			}
			break;
	};
}