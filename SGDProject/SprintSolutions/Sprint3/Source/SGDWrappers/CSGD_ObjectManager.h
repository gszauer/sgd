////////////////////////////////////////////////
//	File	:	"CSGD_ObjectManager.h"
//
//	Author	:	David Brown (DB)
//
//	Purpose	:	To contain and manage all of our game objects.
/////////////////////////////////////////////////
#pragma once
#include <vector>
using std::vector;

class CBase;

class CSGD_ObjectManager
{
private:
	vector<CBase*> m_vObjectList;

	CSGD_ObjectManager(void);
	CSGD_ObjectManager(const CSGD_ObjectManager&);
	CSGD_ObjectManager& operator=(const CSGD_ObjectManager&);
	~CSGD_ObjectManager(void);


public:

	void UpdateObjects(float fElapsedTime);
	void RenderObjects(void);

	static CSGD_ObjectManager* GetInstance();
	static void DeleteInstance();

	void AddObject(CBase* pObject);
	void RemoveObject(CBase* pObject);

	void RemoveAllObjects(void);
	void CheckCollision();
};