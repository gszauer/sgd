Game class -> PROPER SINGLETON!!!
	Initialize
	Execute
		Input
		Update
		Draw
	Cleanup
Game State -> Singleton!
	Enter
	Execute
	Exit
Stack Based State System
	Needs a vector of IGameState Pointers
	size - 1 is the "top" state
	From bottom to top, call all renders
	Only Top recieves input
	Needs a PUSH / POP state
	Needs ClearAllStates
Component interaction Matrix
Reference counting
	Any time an object recieves a reference to another object it calls the AddRef method of the pointer
	When the object is done with the pointer, it must call Release
Event / Message system (Observer / Subscriber" pattern
	When an object needs to send data to another object, it posts an event
	All objects subscribed to that event get the event
Messages
	Handled in one place by message proc
	Can pass any information in message
	Use to create / Destroy objects
	use to communicate with the game state
Events
	Sends a notification to all objects who listen
	Can only have a void pointer attached
	Use for input
	Use to communicate with game objects