#ifndef _H_SGD_WRAPPERS_
#define _H_SGD_WRAPPERS_

#include "SGDWrappers/CSGD_Direct3D.h"
#include "SGDWrappers/CSGD_DirectInput.h"
#include "SGDWrappers/CSGD_DirectSound.h"
#include "SGDWrappers/CSGD_EventSystem.h"
#include "SGDWrappers/CSGD_MessageSystem.h"
#include "SGDWrappers/CSGD_ObjectFactory.h"
#include "SGDWrappers/CSGD_ObjectManager.h"
#include "SGDWrappers/CSGD_TextureManager.h"
#include "SGDWrappers/CSGD_WaveManager.h"
#include "SGDWrappers/SGD_Math.h"
#include "SGDWrappers/SGD_Util.h"
#include "SGDWrappers/CEvent.h"
#include "SGDWrappers/IListener.h"
#include "SGDWrappers/Messages.h"
#include <string>
using std::string;

#endif