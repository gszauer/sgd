#ifndef _H_CBASE_
#define _H_CBASE_

//#include "IBaseInterface.h"
#include <windows.h>

enum GameObjects {OBJ_BASE=0, OBJ_MAX};

class CBase /*: public IBaseInterface*/ {
private:
	float m_nXPosition;
	float m_nYPosition;
	float m_nXVelocity;
	float m_nYVelocity;
	int m_nTextureId;
	unsigned int m_nRefCount;
	int m_nHeight;
	int m_nWidth;
	bool m_bIsActive;
protected:
	int m_nType;

public:
	CBase();
	virtual ~CBase();
	virtual void Update(float fDeltaTime);
	virtual void Render();
	virtual bool CheckCollision(CBase* pBase);

	void AddRef() {
		m_nRefCount++;
	}
	void Release() {
		m_nRefCount--; 
		if (m_nRefCount == 0) delete this;
	}

	RECT GetCollisionRect();
	float GetXPosition() {return m_nXPosition;}
	float GetYPosition() {return m_nYPosition;}
	float GetXVelocity() {return m_nXVelocity;}
	float GetYVelocity() {return m_nYVelocity;}
	int GetTextureId() {return m_nTextureId;}
	unsigned int GetRefCount() {return m_nRefCount;}
	int GetHeight() {return m_nHeight;}
	int GetWidth() {return m_nWidth;}
	bool IsActive() {return m_bIsActive;}

	int GetType() {return m_nType;}
	
	void SetXPosition(float nX) {m_nXPosition = nX;}
	void SetYPosition(float nY) {m_nYPosition = nY;}
	void SetXVelocity(float nX) {m_nXVelocity = nX;}
	void SetYVelocity(float nY) {m_nYVelocity = nY;}
	void SetTextureId(int nId) {m_nTextureId = nId;}
	void SetRefCount(unsigned int nC) {m_nRefCount = nC;}
	void SetHeight(int nH) {m_nHeight = nH;}
	void SetWidth(int nW) {m_nWidth = nW;}
	void SetActive(bool bActive) {m_bIsActive = bActive;}
};

#endif