#ifndef _H_CCURVE_
#define _H_CCURVE_

#include "CBase.h"
#include "math.h"

struct tPoint {
	int m_nX, m_nY;

	void SetX(int nX) {m_nX = nX;}
	void SetY(int nY) {m_nY = nY;}
	int GetX() {return m_nX;}
	int GetY() {return m_nY;}

	tPoint(int nX = 0, int nY = 0) {
		m_nX = nX;
		m_nY = nY;
	}
};

class CCubicCurve : public CBase {
private:
	tPoint m_tPointA;
	tPoint m_tPointB;
	tPoint m_tPoint1;
	tPoint m_tPoint2;
	tPoint m_tLastPoint;
	float m_nT;
	double m_nX, m_nY;

public:
	CCubicCurve() {
		SetXVelocity(0);
		SetYVelocity(0);
		SetXPosition(0);
		SetYPosition(0);
		SetWidth(640);
		SetHeight(480);

		m_tPointA = tPoint(500, 100);
		m_tPointB = tPoint(500, 400);
		m_tPoint1 = tPoint(600, 200);
		m_tPoint2 = tPoint(600, 300);
	}
	void SetPointA(int x, int y) {
		m_tPointA = tPoint(x, y);
	}
	void SetPointB(int x, int y) {
		m_tPointB = tPoint(x, y);
	}
	void SetConrtol1(int x, int y) {
		m_tPoint1 = tPoint(x, y);
	}
	void SetConrtol2(int x, int y) {
		m_tPoint2 = tPoint(x, y);
	}

	void Render() {
		CSGD_Direct3D::GetInstance()->DrawText("Cubic Bezier Curve", m_tPointA.GetX(), m_tPointA.GetY() - 18);
		CSGD_Direct3D::GetInstance()->LineBegin();

		CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.GetX(),m_tPointA.GetY(), m_tPointB.GetX(), m_tPointB.GetY(), 0, 0, 0);
		CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.GetX(),m_tPointA.GetY(), m_tPoint1.GetX(), m_tPoint1.GetY(), 0, 255, 0);
		CSGD_Direct3D::GetInstance()->DrawLine(m_tPointB.GetX(),m_tPointB.GetY(), m_tPoint2.GetX(), m_tPoint2.GetY(), 0, 255, 0);
		
		m_tLastPoint = m_tPointA;

		m_nT = 0;
		for(int i = 0; i <= 50; i++) {
			m_nX = pow(1 - m_nT, 3) * m_tPointA.GetX() + 3.0f * pow(1 - m_nT, 2) * m_nT * m_tPoint1.GetX() + 3.0 * (1 - m_nT) * m_nT * m_nT * m_tPoint2.GetX() + m_nT * m_nT * m_nT * m_tPointB.GetX();
			m_nY = pow(1 - m_nT, 3) * m_tPointA.GetY() + 3.0f * pow(1 - m_nT, 2) * m_nT * m_tPoint1.GetY() + 3.0 * (1 - m_nT) * m_nT * m_nT * m_tPoint2.GetY() + m_nT * m_nT * m_nT * m_tPointB.GetY();

			CSGD_Direct3D::GetInstance()->DrawLine(m_tLastPoint.GetX(),m_tLastPoint.GetY(), m_nX, m_nY, 255, 0, 0);

			m_nT += 1.0f / 50;
			m_tLastPoint.SetX(m_nX);
			m_tLastPoint.SetY(m_nY);
		}

		CSGD_Direct3D::GetInstance()->LineEnd();
	}
};

class CQuadraticCurve : public CBase {
private:
	tPoint m_tPointA;
	tPoint m_tPointB;
	tPoint m_tPoint1;
	tPoint m_tLastPoint;
	float m_nT;
	double m_nX, m_nY;

public:
	CQuadraticCurve() {
		SetXVelocity(0);
		SetYVelocity(0);
		SetXPosition(0);
		SetYPosition(0);
		SetWidth(640);
		SetHeight(480);

		m_tPointA = tPoint(225, 300);
		m_tPointB = tPoint(425, 300);
		m_tPoint1 = tPoint(325, 250);
	}
	void Render() {
		CSGD_Direct3D::GetInstance()->DrawText("Quadratic Bezier Curve", m_tPointA.GetX(), m_tPointA.GetY() + 5);
		CSGD_Direct3D::GetInstance()->LineBegin();

		CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.GetX(),m_tPointA.GetY(), m_tPointB.GetX(), m_tPointB.GetY(), 0, 0, 0);
		CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.GetX(),m_tPointA.GetY(), m_tPoint1.GetX(), m_tPoint1.GetY(), 0, 255, 0);
		CSGD_Direct3D::GetInstance()->DrawLine(m_tPointB.GetX(),m_tPointB.GetY(), m_tPoint1.GetX(), m_tPoint1.GetY(), 0, 255, 0);
		
		m_tLastPoint = m_tPointA;

		m_nT = 0;
		for(int i = 0; i <= 50; i++) {
			m_nX = pow(1 - m_nT, 2) * m_tPointA.GetX() + 2.0f * (1 - m_nT) * m_nT * m_tPoint1.GetX() + m_nT * m_nT * m_tPointB.GetX();
			m_nY = pow(1 - m_nT, 2) * m_tPointA.GetY() + 2.0f * (1 - m_nT) * m_nT * m_tPoint1.GetY() + m_nT * m_nT * m_tPointB.GetY();

			CSGD_Direct3D::GetInstance()->DrawLine(m_tLastPoint.GetX(),m_tLastPoint.GetY(), m_nX, m_nY, 255, 0, 0);

			m_nT += 1.0f / 50;
			m_tLastPoint.SetX(m_nX);
			m_tLastPoint.SetY(m_nY);
		}

		CSGD_Direct3D::GetInstance()->LineEnd();
	}
};

#endif