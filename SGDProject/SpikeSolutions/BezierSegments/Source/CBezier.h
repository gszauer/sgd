#ifndef _H_CBEZIER_
#define _H_CBEZIER_

#include "SGDWrappers/CSGD_Direct3D.h"
#include <cmath>
#include <vector>
using std::vector;

#define _CBEZIER_NUM_RENDER_STEPS_ 100

struct TPoint {
	int x, y;
	TPoint() : x(0), y(0) {}
};

struct TBezierPoint : public TPoint {
	TPoint control;
	TBezierPoint() : TPoint() {}
};

class CBezierSegment {
private:
	TBezierPoint m_tPointA;
	TBezierPoint m_tPointB;
	TBezierPoint m_tStartPoint;

	int m_nSize;
	float m_nT;
	int m_nX, m_nY;
	bool m_bShowLine;
	bool m_bShowGreenLine;
	bool m_bShowRedLine;
public:
	CBezierSegment(TBezierPoint tPointA, TBezierPoint tPointB) {}
	CBezierSegment(int nX1 = 0, int nY1 = 0, int nC1 = 0, int nC2 = 0, int nX2 = 0, int nY2 = 0, int nC3 = 0, int nC4 = 0) {
		m_tPointA.x = nX1;
		m_tPointA.y = nY1;
		m_tPointA.control.x = nC1;
		m_tPointA.control.y = nC2;
		m_tPointB.x = nX2;
		m_tPointB.y = nY2;
		m_tPointB.control.x = nC3;
		m_tPointB.control.y = nC4;
		m_nSize = 0;
	}
	TBezierPoint* GetPointAPointer() {return &m_tPointA;}
	TBezierPoint* GetPointBPointer() {return &m_tPointB;}
	void SetPointA(TBezierPoint tPointA) {m_tPointA = tPointA;}
	void SetPointB(TBezierPoint tPointB) {m_tPointB = tPointB;}
	void AddPoint(TBezierPoint tPoint) {
		if (m_nSize == 0) {
			m_tPointA = tPoint;
			m_nSize++;
		} else {
			m_tPointB = tPoint;
			m_nSize++;
		}
	}
	void BlackLineVisible(bool bVisible) {m_bShowLine = bVisible;}
	void GreenLineVisible(bool bVisible) {m_bShowGreenLine = bVisible;}
	void RedLineVisible(bool bVisible) {m_bShowRedLine = bVisible;}
	void Render() {
		CSGD_Direct3D::GetInstance()->LineBegin();
		m_nSize = m_vBezierPoints.size();
		if (m_nSize == 0) {
			// Render nothing!
		} else if (m_nSize == 1) {
			CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.x, m_tPointA.y, m_tPointA.control.x, m_tPointA.control.y, 0, 255, 0);
		} else if (m_nSize > 1) {
			if (m_bShowLine)
				CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.x, m_tPointA.y, m_tPointB.x, m_tPointB.y, 0, 0, 0);
			if (m_bShowGreenLine) {
				CSGD_Direct3D::GetInstance()->DrawLine(m_tPointA.x, m_tPointA.y, m_tPointA.control.x, m_tPointA.control.y, 0, 255, 0);
				CSGD_Direct3D::GetInstance()->DrawLine(m_tPointB.x, m_tPointB.y, m_tPointB.control.x, m_tPointB.control.y, 0, 255, 0);
			}
			m_tStartPoint = m_tPointA;
			for (int j = 0; j < _CBEZIER_NUM_RENDER_STEPS_; ++j) {
				m_nX = pow(1 - m_nT, 3) * m_tStartPoint.x + 3.0f * pow(1 - m_nT, 2) * m_nT * m_tStartPoint.control.x + 3.0 * (1 - m_nT) * m_nT * m_nT * m_tPointB.control.x + m_nT * m_nT * m_nT * m_tPointB.x;
				m_nY = pow(1 - m_nT, 3) * m_tStartPoint.y + 3.0f * pow(1 - m_nT, 2) * m_nT * m_tStartPoint.control.y + 3.0 * (1 - m_nT) * m_nT * m_nT * m_tPointB.control.y + m_nT * m_nT * m_nT * m_tPointB.y;

				if (m_bShowRedLine)
					CSGD_Direct3D::GetInstance()->DrawLine(m_tStartPoint.x, m_tStartPoint.y, m_nX, m_nY, 255, 0, 0);

				m_nT += 1.0f / (float)_CBEZIER_NUM_RENDER_STEPS_;
				m_tStartPoint.x = m_nX;
				m_tStartPoint.y = m_nY;
			}
		} else {
			// This is a hickup....
		}
		CSGD_Direct3D::GetInstance()->LineEnd();
	}
};

class CBezierSegments {
private:
	vector<CBezierSegment> m_tLineSegments;
	int m_nSize;
public:

};

class CBezierPoints {
private:
	vector<TBezierPoint> m_vBezierPoints;
	int m_nSize;
	TBezierPoint m_tStartPoint;
	float m_nT;
	int m_nX, m_nY;
	bool m_bShowLine;
	bool m_bShowGreenLine;
	bool m_bShowRedLine;
public:
	CBezierPoints() {
		m_bShowLine = false;
		m_bShowGreenLine = false;
		m_bShowRedLine = false;
	}

	void BlackLineVisible(bool bVisible) {m_bShowLine = bVisible;}
	void GreenLineVisible(bool bVisible) {m_bShowGreenLine = bVisible;}
	void RedLineVisible(bool bVisible) {m_bShowRedLine = bVisible;}
	TBezierPoint* GetLastItemPointer() {return &(m_vBezierPoints[m_vBezierPoints.size() - 1]);}


	void AddPoint(TBezierPoint tPoint) {
		m_vBezierPoints.push_back(tPoint);
	}

	void Render() {
		CSGD_Direct3D::GetInstance()->LineBegin();
		m_nSize = m_vBezierPoints.size();
		if (m_nSize == 0) {
			// Render nothing!
		} else if (m_nSize == 1) {
			CSGD_Direct3D::GetInstance()->DrawLine(m_vBezierPoints[0].x, m_vBezierPoints[0].y, m_vBezierPoints[0].control.x, m_vBezierPoints[0].control.y, 0, 255, 0);
		} else if (m_nSize > 1) {
			m_tStartPoint = m_vBezierPoints[0];
			for (int i = 1; i < m_vBezierPoints.size(); ++i) {
				m_nT = 0;
				
				if (m_bShowLine)
					CSGD_Direct3D::GetInstance()->DrawLine(m_tStartPoint.x, m_tStartPoint.y, m_vBezierPoints[i].x, m_vBezierPoints[i].y, 0, 0, 0);
				if (m_bShowGreenLine) {
					CSGD_Direct3D::GetInstance()->DrawLine(m_tStartPoint.x, m_tStartPoint.y, m_tStartPoint.control.x, m_tStartPoint.control.y, 0, 255, 0);
					CSGD_Direct3D::GetInstance()->DrawLine(m_vBezierPoints[i].x, m_vBezierPoints[i].y, m_vBezierPoints[i].control.x, m_vBezierPoints[i].control.y, 0, 255, 0);
				}

				for (int j = 0; j < _CBEZIER_NUM_RENDER_STEPS_; ++j) {
					m_nX = pow(1 - m_nT, 3) * m_tStartPoint.x + 3.0f * pow(1 - m_nT, 2) * m_nT * m_tStartPoint.control.x + 3.0 * (1 - m_nT) * m_nT * m_nT * m_vBezierPoints[i].control.x + m_nT * m_nT * m_nT * m_vBezierPoints[i].x;
					m_nY = pow(1 - m_nT, 3) * m_tStartPoint.y + 3.0f * pow(1 - m_nT, 2) * m_nT * m_tStartPoint.control.y + 3.0 * (1 - m_nT) * m_nT * m_nT * m_vBezierPoints[i].control.y + m_nT * m_nT * m_nT * m_vBezierPoints[i].y;

					if (m_bShowRedLine)
						CSGD_Direct3D::GetInstance()->DrawLine(m_tStartPoint.x, m_tStartPoint.y, m_nX, m_nY, 255, 0, 0);

					m_nT += 1.0f / (float)_CBEZIER_NUM_RENDER_STEPS_;
					m_tStartPoint.x = m_nX;
					m_tStartPoint.y = m_nY;
				}
			}
		} else {
			// This is a hickup....
		}
		CSGD_Direct3D::GetInstance()->LineEnd();
	}
};

#endif