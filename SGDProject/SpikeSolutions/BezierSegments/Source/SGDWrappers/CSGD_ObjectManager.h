////////////////////////////////////////////////
//	File	:	"CSGD_ObjectManager.h"
//
//	Author	:	David Brown (DB)
//
//	Purpose	:	To contain and manage all of our game objects.
/////////////////////////////////////////////////
#pragma once
#include <vector>
using std::vector;

class CBase;

class CSGD_ObjectManager
{
private:
	vector<CBase*> m_vObjectList;
	vector<CBase*> m_vDeadObjects;

	CSGD_ObjectManager(void);
	~CSGD_ObjectManager(void);
	CSGD_ObjectManager(const CSGD_ObjectManager& rObject) {}
	CSGD_ObjectManager& operator=(const CSGD_ObjectManager& RenderObject) {}
public:
	static CSGD_ObjectManager* GetInstance();

	void UpdateObjects(float fDelta);
	void RenderObjects(void);
	void CheckCollisions();

	void AddObject(CBase* pObject);
	void RemoveObject(CBase* pObject);

	void RemoveAllObjects(void);
};