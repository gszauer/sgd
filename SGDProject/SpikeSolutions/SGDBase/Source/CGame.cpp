#include "CGame.h"

bool CGame::Input() {
	m_pDI->ReadDevices(); 

	if ((m_pDI->KeyDown(DIK_LALT) || m_pDI->KeyDown(DIK_RALT)) && m_pDI->KeyPressed(DIK_RETURN)) {
		m_bWindowed = !m_bWindowed;
		m_pD3D->ChangeDisplayParam(m_nScreenW, m_nScreenH, m_bWindowed);
		ShowCursor(((m_bWindowed)? TRUE : FALSE));
		return true;
	}

	return true;
}

void CGame::Update() {

}

void CGame::Render() {
	m_pD3D->Clear(0, 0, 255);
	m_pD3D->DeviceBegin();
	m_pD3D->SpriteBegin();

	//m_pCurState->Render();
	
	m_pD3D->SpriteEnd();
	m_pD3D->DrawText("SGD Spike Solution", 260, 20);
	m_pD3D->DeviceEnd();
	m_pD3D->Present();
}

///////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////
CGame::CGame() {
	m_pD3D = NULL;
	m_pTM  = NULL;
	m_pDS  = NULL;
	m_pWM  = NULL;
	m_pDI  = NULL;
}
CGame::~CGame() {
	if(m_pDI  != NULL) {
		m_pDI->ShutdownDirectInput();
		m_pDI = NULL;
	}
	if(m_pWM  != NULL) {
		m_pWM->ShutdownWaveManager();
		m_pWM = NULL;
	}
	if(m_pDS  != NULL) {
		m_pDS->ShutdownDirectSound();
		m_pDS = NULL;
	}
	if(m_pTM  != NULL) {
		m_pTM->ShutdownTextureManager();
		m_pTM = NULL;
	}
	if(m_pD3D != NULL) {
		m_pD3D->ShutdownDirect3D();
		m_pD3D = NULL;
	}
}
CGame* CGame::GetInstance() {
	static CGame instance;
	return &instance;
}
void CGame::Initialize(HWND hWnd, HINSTANCE hInstance, int nScreenWidth, int nScreenHeight, bool bIsWindowed) {
	m_pD3D = CSGD_Direct3D::GetInstance();
	m_pTM  = CSGD_TextureManager::GetInstance();
	m_pDS  = CSGD_DirectSound::GetInstance();
	m_pWM  = CSGD_WaveManager::GetInstance();
	m_pDI  = CSGD_DirectInput::GetInstance();

	m_pD3D->InitDirect3D(hWnd, nScreenWidth, nScreenHeight, bIsWindowed, true); // Limit to 60 fps?
	m_pTM->InitTextureManager(m_pD3D->GetDirect3DDevice(), m_pD3D->GetSprite());
	m_pDS->InitDirectSound(hWnd);
	m_pWM->InitWaveManager(hWnd, m_pDS->GetDSObject());
	m_pDI->InitDirectInput(hWnd, hInstance, DI_KEYBOARD);

	m_hWnd = hWnd;

	m_bWindowed = bIsWindowed;
}
bool CGame::Main() {
	if (!Input())
		return false;
	Update();
	Render();
	return true;
}
void CGame::Shutdown() {
	if(m_pDI  != NULL) {
		m_pDI->ShutdownDirectInput();
		m_pDI = NULL;
	}
	if(m_pWM  != NULL) {
		m_pWM->ShutdownWaveManager();
		m_pWM = NULL;
	}
	if(m_pDS  != NULL) {
		m_pDS->ShutdownDirectSound();
		m_pDS = NULL;
	}
	if(m_pTM  != NULL) {
		m_pTM->ShutdownTextureManager();
		m_pTM = NULL;
	}
	if(m_pD3D != NULL) {
		m_pD3D->ShutdownDirect3D();
		m_pD3D = NULL;
	}
}