#ifndef _H_IBASE_INTERFACE_
#define _H_IBASE_INTERFACE_

class IBaseInterface {
public:
	virtual ~IBaseInterface() = 0{};

	virtual void Update() = 0;
	virtual void Render() = 0;

	virtual void AddRef() = 0;
	virtual void Release() = 0;
};

#endif