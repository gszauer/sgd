#ifndef _H_CGAME_
#define _H_CGAME_

#include "SGDWrappers.h"
#include "CBezier.h"
#include <windows.h>

class CGame {
private:
	CSGD_Direct3D*			m_pD3D;
	CSGD_TextureManager*	m_pTM;
	CSGD_DirectSound*		m_pDS;
	CSGD_WaveManager*		m_pWM;
	CSGD_DirectInput*		m_pDI;
	
	bool m_bWindowed;
	int m_nScreenW;
	int m_nScreenH;

	CBezierCurve m_cLevel;
	int m_nMouseX;
	int m_nMouseY;
	int m_nTempX;
	int m_nTempY;
	POINT m_pMouse;
	bool m_bCordsSet;
	HWND m_hWnd;

	bool Input();
	void Update();
	void Render();

	CGame();
	CGame(const CGame& rCGame) {}
	CGame& operator=(const CGame& rCGame){}
	~CGame();
public:
	static CGame* GetInstance();
	void Initialize(HWND hWnd, HINSTANCE hInstance, int nScreenWidth, int nScreenHeight, bool bIsWindowed);
	bool Main();
	void Shutdown();

	void SetScreenWidth(int nWidth) {m_nScreenW = nWidth;}
	void SetScreenHeight(int nHeight) {m_nScreenH = nHeight;}
};

#endif