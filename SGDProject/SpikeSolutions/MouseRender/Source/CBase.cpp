#include "CBase.h"
#include "SGDWrappers/CSGD_TextureManager.h"

CBase::CBase() {
	m_nXPosition = 0;
	m_nYPosition = 0;
	m_nXVelocity = 0;
	m_nYVelocity = 0;
	m_nTextureId = 0;
	m_nRefCount = 1;
	m_nHeight = 0;
	m_nWidth = 0;
	m_bIsActive = true;
	m_nType = OBJ_BASE;
}

CBase::~CBase() {

}

RECT CBase::GetCollisionRect() {
	RECT rTemp;
	SetRect(&rTemp, GetXPosition(), GetYPosition(), GetXPosition() + GetWidth(), GetYPosition() + GetHeight());
	return rTemp;
}

void CBase::Update(float fDelta) {
	SetXPosition(GetXPosition() + GetXVelocity() * fDelta);
	SetYPosition(GetYPosition() + GetYVelocity() * fDelta);
}

void CBase::Render() {
	CSGD_TextureManager::GetInstance()->Draw(GetTextureId(), GetXPosition(), GetYPosition());
}

bool CBase::CheckCollision(CBase* pBase) {
	RECT tRect;
	return (bool)IntersectRect(&tRect, &GetCollisionRect(), &pBase->GetCollisionRect());
}