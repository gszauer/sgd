#ifndef _H_MESSAGE_
#define _H_MESSAGE_

typedef int MSGID;

enum eMsgTypes { MSG_NULL = 0, MSG_MAX };

class CBaseMessage
{
private:
	MSGID	m_msgID;

public:
	CBaseMessage(MSGID msgID)
	{
		m_msgID = msgID;
	}

	virtual ~CBaseMessage(void) {}

	MSGID GetMsgID(void)	{ return m_msgID; }
};

#endif