#include "CPath.h"
#include <cmath>
#include "VectUtils.h"
#include "C2DMatrix.h"

inline void Vec2DRotateAroundOrigin(Vector2D& v, double ang) {
	C2DMatrix mat;
	mat.Rotate(ang);
	mat.TransformVector2Ds(v);
}

vector<Vector2D> CPath::CreateRandomPath(int nNumWayPoints, double nMinX, double nMinY, double nMaxX, double nMaxY) {
	m_vWayPoints.clear();

	double midX = (nMaxX+nMinX)/2.0;
	double midY = (nMaxY+nMinY)/2.0;
	double smaller = min(midX, midY);
	double spacing = TwoPi/(double)nNumWayPoints;
	for (int i=0; i<nNumWayPoints; ++i) {
		double RadialDist = RandInRange(smaller*0.2f, smaller);
		Vector2D temp(RadialDist, 0.0f);
		Vec2DRotateAroundOrigin(temp, i*spacing);
		temp.x += midX; temp.y += midY;
		m_vWayPoints.push_back(temp);
	}

	m_nCurWayPoint = 0;
	return m_vWayPoints;
}


void CPath::Render() const {
	CSGD_Direct3D::GetInstance()->LineBegin();
	int nNumPoints = m_vWayPoints.size();
	Vector2D vStartPoint = m_vWayPoints[0];
	for (int i = 1; i < nNumPoints; i++) {
		CSGD_Direct3D::GetInstance()->DrawLine(vStartPoint.x,vStartPoint.y, m_vWayPoints[i].x, m_vWayPoints[i].y, 0, 0, 0);
		CSGD_Direct3D::GetInstance()->DrawLine(vStartPoint.x - 5, vStartPoint.y - 5, vStartPoint.x + 5, vStartPoint.y + 5, 255, 0, 0);
		CSGD_Direct3D::GetInstance()->DrawLine(vStartPoint.x + 5, vStartPoint.y - 5, vStartPoint.x - 5, vStartPoint.y + 5, 255, 0, 0);
		
		vStartPoint = m_vWayPoints[i];
	}
	CSGD_Direct3D::GetInstance()->DrawLine(vStartPoint.x,vStartPoint.y, m_vWayPoints[0].x, m_vWayPoints[0].y, 0, 0, 0);
	CSGD_Direct3D::GetInstance()->DrawLine(m_vWayPoints[nNumPoints - 1].x - 5, m_vWayPoints[nNumPoints - 1].y - 5, m_vWayPoints[nNumPoints - 1].x + 5, m_vWayPoints[nNumPoints - 1].y + 5, 255, 0, 0);
	CSGD_Direct3D::GetInstance()->DrawLine(m_vWayPoints[nNumPoints - 1].x + 5, m_vWayPoints[nNumPoints - 1].y - 5, m_vWayPoints[nNumPoints - 1].x - 5, m_vWayPoints[nNumPoints - 1].y + 5, 255, 0, 0);
	CSGD_Direct3D::GetInstance()->LineEnd();
}
