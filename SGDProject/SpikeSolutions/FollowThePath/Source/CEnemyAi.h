#ifndef _H_CEMENYAI_
#define _H_CENEMYAI_

#include <windows.h>
#include "Vector2D.h"
#include "CBase.h"
#include <vector>
#include "CPath.h"
using std::vector;

class CAi {
private:
	int m_nXPosition;
	int m_nYPosition;
	int m_nXVelocity;
	int m_nYVelocity;
	unsigned int m_nRefCount;
	int m_nHeight;
	int m_nWidth;
	float m_nMaxSpeed;
	Vector2D m_vTarget;
public:
	CAi();
	~CAi();
	void Update();
	void Render();
	void Seek(int, int);
	void FollowPath(CPath* pPath);
	void AddRef() {
		m_nRefCount++;
	}
	void Release() {
		m_nRefCount--; 
		if (m_nRefCount == 0) delete this;
	}

	RECT GetCollisionRect();
	//virtual bool CheckCollision(CBase* pBase);
	int GetXPosition() {return m_nXPosition;}
	int GetYPosition() {return m_nYPosition;}
	int GetXVelocity() {return m_nXVelocity;}
	int GetYVelocity() {return m_nYVelocity;}
	unsigned int GetRefCount() {return m_nRefCount;}
	int GetHeight() {return m_nHeight;}
	int GetWidth() {return m_nWidth;}

	void SetXPosition(int nX) {m_nXPosition = nX;}
	void SetYPosition(int nY) {m_nYPosition = nY;}
	void SetXVelocity(int nX) {m_nXVelocity = nX;}
	void SetYVelocity(int nY) {m_nYVelocity = nY;}
	void SetRefCount(unsigned int nC) {m_nRefCount = nC;}
	void SetHeight(int nH) {m_nHeight = nH;}
	void SetWidth(int nW) {m_nWidth = nW;}
};

#endif