Gabor Szauer

Controls:
	Main Menu
		Up / Down to navigate the menu
		Enter To select
	Credits screen
		Escape to exit
	Game Play
		W / A / S / D To Move
		Space to use item
		1 to get green shell (item)
		2 to get red shell (item)
		3 to get blue shell (item)
		Escape / P to pause
	Pause Menu
		Up / Down to navigate the menu
		Enter to select
		Esc / Pause to return to game

No Powerups
No cheats
No boss stages

Feature Points:
	2 Point features
			Different Enemy Types - Enemies have different weight / speed
			Physics based movement - Friction and weight affect acceleration
			Steering - Both for cart and shell movement
	3 point features	
			Multiple weapons - Green, red, and blue shells
			WayPoints - Enemy ai follows way points
Total feature points: 12

No events

Messages:
Create red shell
Create green shell
Create blue shell
Destroy red shell
Destroy green shell
Destroy blue shell
kartspinout