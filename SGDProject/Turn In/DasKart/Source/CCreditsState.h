#ifndef _H_CCREDITS_
#define _H_CCREDITS_

#include "IGameState.h"
#include "CGame.h"
#include <windows.h>
#include "SGDWrappers.h"

class CCreditsState : public IGameState {
private:
	CCreditsState();
	~CCreditsState();
	CCreditsState(const CCreditsState&) {}
	CCreditsState& operator=(const CCreditsState&) {}

	CSGD_Direct3D*		 m_pD3D;
	CSGD_DirectInput*	 m_pDI;
	CSGD_TextureManager* m_pTM;
	CSGD_WaveManager*	 m_pWM;
	CSGD_DirectSound*	 m_pDS;

	int m_nMenuBg;
	bool m_bInitialized;
public:
	void Enter(void);
	bool Input(void);
	void Update(float fDelta);
	void Render(void);
	void Exit(void);
	static CCreditsState* GetInstance();
};

#endif