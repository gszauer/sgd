#ifndef _H_CTIMER_
#define _H_CTIMER_

#include <windows.h>

//////////////////////////////////////////////////////////////////////////
// Usage: 
// Notes:
// This WILL NOT limit FPS. Delta time is a scalar value, multiply the
// game update by the return on GetDelta(); this will make the objects
// move in a way that makes it look like each update only happens once 
// a second.
//////////////////////////////////////////////////////////////////////////

class CTimer {
private:
	DWORD	m_dwPrevTimeStamp;	// The time the timer had last frame
	DWORD	m_dwStartTimeStamp;	// The time when update was called
	DWORD	m_dwFPSTimeStamp;	// Gets set once every second
	float	m_nElapsedTime;		// Time (in seconds) passed between frames
	float	m_nGameTime;		// How long the game has been running
	int		m_nFrameCount;		// Increments once every frame
	int		m_nFPS;				// The Frames Per Second of our game
	
	CTimer();
	~CTimer() {}
	CTimer(const CTimer& rCTimer) {}
	CTimer& operator=(const CTimer& rCTimer) {}
public:
	static CTimer* GetInstance();
	void Initialize();
	void Update();
	float GetGameTime();
	float GetDelta();
	float GetFps();
};

#endif