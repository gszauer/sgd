#ifndef _H_BLUESHELL_
#define _H_BLUESHELL_

#include "CBaseObject.h"
#include "CGamePlayState.h"
#include "CKartObject.h"

class CBlueShell : public CBase {
private:
	tVector2D		m_vThrust;
	tVector2D		m_vDifference;
	float			m_nAngle;
	float			m_nSpeed;
	CKartObject*	m_pOriginObject;
	CKartObject*	m_pTargetObject;
public:
	void Update(float nDelta);
	void Render();

	CBlueShell() {m_nType = ITEM_BLUESHELL;}
	float GetXThrust() {return m_vThrust.fX;}
	float GetYThrust() {return m_vThrust.fY;}
	tVector2D GetThrustVector() {return m_vThrust;}
	float GetCurrentAngle() {return m_nAngle;}
	float GetCurrentSpeed() {return m_nSpeed;}
	CKartObject* GetOriginObject() {return m_pOriginObject;}
	CKartObject* GetTargetObject() {return m_pTargetObject;}

	void SetXThrust(float nX) {m_vThrust.fX = nX;}
	void SetYThrust(float nY) {m_vThrust.fY = nY;}
	void SetThrustVector(tVector2D v) {m_vThrust = v;}
	void SetCurrentAngle(float nA) {m_nAngle = nA;}
	void SetCurrentSpeed(float nS) {m_nSpeed = nS;}
	void SetOriginKart(CKartObject* p) {m_pOriginObject = p;}
	void SetTargetObject(CKartObject* p) {m_pTargetObject = p;}

	RECT GetCollisionRect() {
		RECT rTemp;
		SetRect(&rTemp, (GetXPosition() - GetWidth()/2), (GetYPosition() - GetHeight()/2), (GetXPosition() - GetWidth()/2) + GetWidth(), (GetYPosition() - GetHeight()/2) + GetHeight());
		return rTemp;
	}

	bool CheckCollision(CBase* pBase) {
		return CBase::CheckCollision(pBase);
	}
};

#endif