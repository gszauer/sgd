#pragma once

typedef int MSGID;
#ifndef NULL
#define NULL 0 
#endif
#define _SHELL_WIDTH_ 10
#define _SHELL_HEIGHT_ 10
#define _SHELL_VELOCITY_X_ 50
#define _SHELL_VELOCITY_Y_ 50
#define _SHELL_SPEED_ 160

class CKartObject;
class CGreenShell;
class CRedShell;
class CBlueShell;

enum eMsgTypes { MSG_NULL = 0, MSG_CREATEGREENSHELL, MSG_DESTROYGREENSHELL, MSG_KARTSPINOUT, MSG_CREATEREDSHELL, MSG_DESTROYREDSHELL,
				 MSG_CREATEBLUESHELL, MSG_DESTROYBLUESHELL, MSG_MAX };

class CBaseMessage {
private:
	MSGID	m_msgID;
public:
	CBaseMessage(MSGID msgID);
	virtual ~CBaseMessage(void) {}
	MSGID GetMsgID(void) {return m_msgID;}
};

class CDestroyBlueShell : public CBaseMessage {
private:
	CBlueShell* m_pTarget;
public:
	CDestroyBlueShell(CBlueShell* pTarget);
	CBlueShell* GetTarget() {return m_pTarget;}
};

class CreateBlueShellMessage : public CBaseMessage {
private:
	float			m_nXPosition;
	float			m_nYPosition;
	float			m_nCurrentAngle;
	float			m_nXVelocity;
	float			m_nYVelocity;
	float			m_nCurrentSpeed;
	float			m_nWidth;
	float			m_nHeight;
	CKartObject*	m_pOrigin;
	CKartObject*	m_pTarget;
public:
	CreateBlueShellMessage (CKartObject* pOrigin);
	float GetXPosition() {return m_nXPosition;}
	float GetYPosition() {return m_nYPosition;}
	float GetCurAngle() {return m_nCurrentAngle;}
	float GetXVelocity() {return m_nXVelocity;}
	float GetYVelocity() {return m_nYVelocity;}
	float GetCurrentSpeed() {return m_nCurrentSpeed;}
	float GetWidth() {return m_nWidth;}
	float GetHeight() {return m_nHeight;}
	CKartObject* GetOrigin() {return m_pOrigin;}
	CKartObject* GetTarget() {return m_pTarget;}
};

class CDestroyRedShell : public CBaseMessage {
private:
	CRedShell* m_pTarget;
public:
	CDestroyRedShell(CRedShell* pTarget);
	CRedShell* GetTarget() {return m_pTarget;}
};

class CreateRedShellMessage : public CBaseMessage {
private:
	float			m_nXPosition;
	float			m_nYPosition;
	float			m_nCurrentAngle;
	float			m_nXVelocity;
	float			m_nYVelocity;
	float			m_nCurrentSpeed;
	float			m_nWidth;
	float			m_nHeight;
	CKartObject*	m_pOrigin;
	CKartObject*	m_pTarget;
public:
	CreateRedShellMessage(CKartObject* pOrigin);
	float GetXPosition() {return m_nXPosition;}
	float GetYPosition() {return m_nYPosition;}
	float GetCurAngle() {return m_nCurrentAngle;}
	float GetXVelocity() {return m_nXVelocity;}
	float GetYVelocity() {return m_nYVelocity;}
	float GetCurrentSpeed() {return m_nCurrentSpeed;}
	float GetWidth() {return m_nWidth;}
	float GetHeight() {return m_nHeight;}
	CKartObject* GetOrigin() {return m_pOrigin;}
};

class CKartSpinoutMessage : public CBaseMessage {
private:
	CKartObject* m_pTarget;
public:
	CKartSpinoutMessage(CKartObject* pTarget);
	CKartObject* GetTarget();
};

class CDestroyGreenShell : public CBaseMessage {
private:
	CGreenShell* m_pTarget;
public:
	CDestroyGreenShell(CGreenShell* pTarget);
	CGreenShell* GetTarget();
};

class CreateGreenShellMessage : public CBaseMessage {
private:
	float			m_nXPosition;
	float			m_nYPosition;
	float			m_nCurrentAngle;
	float			m_nXVelocity;
	float			m_nYVelocity;
	float			m_nCurrentSpeed;
	float			m_nWidth;
	float			m_nHeight;
	CKartObject*	m_pOriginObject;
public:
	CreateGreenShellMessage(CKartObject* pOrigin);
	float GetXPosition() {return m_nXPosition;}
	float GetYPosition() {return m_nYPosition;}
	float GetCurAngle() {return m_nCurrentAngle;}
	float GetXVelocity() {return m_nXVelocity;}
	float GetYVelocity() {return m_nYVelocity;}
	float GetCurrentSpeed() {return m_nCurrentSpeed;}
	float GetWidth() {return m_nWidth;}
	float GetHeight() {return m_nHeight;}
	CKartObject* GetOrigin() {return m_pOriginObject;}
};