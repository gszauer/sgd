#include "Messages.h"
#include "../CKartObject.h"
#include "../CGreenShell.h"
#include "../CRedShell.h"
#include "../CBlueShell.h"
#include "../CGamePlayState.h"
#include "../CKartManager.h"
#include "SGD_Math.h"

CBaseMessage::CBaseMessage(MSGID msgID) {
	m_msgID = msgID;
}

CDestroyBlueShell::CDestroyBlueShell(CBlueShell* pTarget)  : CBaseMessage(MSG_DESTROYBLUESHELL) {
	m_pTarget = pTarget;
}

CreateBlueShellMessage::CreateBlueShellMessage(CKartObject* pOrigin) : CBaseMessage(MSG_CREATEBLUESHELL) {
	static float nAngle;
	static tVector2D vDifference;
	static tVector2D tUpVector = {0, -1};

	m_pOrigin = pOrigin;
	m_pTarget = CKartManager::GetInstance()->GetFirstKart();
	vDifference = m_pTarget->GetPositionVector() - m_pOrigin->GetPositionVector();
	nAngle = AngleBetweenVectors(tUpVector, vDifference);

	m_nXPosition = pOrigin->GetXPosition();
	m_nYPosition = pOrigin->GetYPosition();
	m_nCurrentAngle = nAngle;
	m_nXVelocity = _SHELL_VELOCITY_X_;
	m_nYVelocity = _SHELL_VELOCITY_Y_;
	m_nCurrentSpeed = _SHELL_SPEED_;
	m_nWidth = _SHELL_WIDTH_;
	m_nHeight = _SHELL_HEIGHT_;

} 

CreateRedShellMessage::CreateRedShellMessage(CKartObject* pOrigin) : CBaseMessage(MSG_CREATEREDSHELL) {
	static float nAngle;
	static tVector2D vDifference;
	static tVector2D tUpVector = {0, -1};
	
	m_pOrigin = pOrigin;
	m_pTarget = CKartManager::GetInstance()->GetNextKart(pOrigin);
	vDifference = m_pTarget->GetPositionVector() - m_pOrigin->GetPositionVector();
	nAngle = AngleBetweenVectors(tUpVector, vDifference);

	m_nXPosition = pOrigin->GetXPosition();
	m_nYPosition = pOrigin->GetYPosition();
	m_nCurrentAngle = nAngle;
	m_nXVelocity = _SHELL_VELOCITY_X_;
	m_nYVelocity = _SHELL_VELOCITY_Y_;
	m_nCurrentSpeed = _SHELL_SPEED_;
	m_nWidth = _SHELL_WIDTH_;
	m_nHeight = _SHELL_HEIGHT_;
			
} 

CKartSpinoutMessage::CKartSpinoutMessage(CKartObject* pTarget)  : CBaseMessage(MSG_KARTSPINOUT) {
	m_pTarget = pTarget;
}

CKartObject* CKartSpinoutMessage::GetTarget() {
	return m_pTarget;
}

CDestroyGreenShell::CDestroyGreenShell(CGreenShell* pTarget)  : CBaseMessage(MSG_DESTROYGREENSHELL) {
	m_pTarget = pTarget;
}

CDestroyRedShell::CDestroyRedShell(CRedShell* pTarget)  : CBaseMessage(MSG_DESTROYREDSHELL) {
	m_pTarget = pTarget;
}

CGreenShell* CDestroyGreenShell::GetTarget() {
	return m_pTarget;
}

CreateGreenShellMessage::CreateGreenShellMessage(CKartObject* pOrigin) : CBaseMessage(MSG_CREATEGREENSHELL) {
	m_nXPosition = pOrigin->GetXPosition();
	m_nYPosition = pOrigin->GetYPosition();
	m_nCurrentAngle = pOrigin->GetRotationAngle();
	m_nXVelocity = _SHELL_VELOCITY_X_;
	m_nYVelocity = _SHELL_VELOCITY_Y_;
	m_nCurrentSpeed = _SHELL_SPEED_;
	m_nWidth = _SHELL_WIDTH_;
	m_nHeight = _SHELL_HEIGHT_;
	m_pOriginObject = pOrigin;			
}