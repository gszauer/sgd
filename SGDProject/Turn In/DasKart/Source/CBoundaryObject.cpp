#include "CBoundaryObject.h"

CBoundaryObject::CBoundaryObject(int nX, int nY, int nW, int nH) : CBase() {
	SetXPosition(nX);
	SetYPosition(nY);
	SetWidth(nW);
	SetHeight(nH);
	m_nType = OBJ_BOUNDARY;
}

void CBoundaryObject::Render() {
	CSGD_Direct3D::GetInstance()->DrawRect(GetCollisionRect(), 125, 125, 0);
}