#ifndef _H_AIMANAGER_
#define _H_AIMANAGER_

#include "CAi.h"
#include <vector>
using std::vector;

class CAiManager {
private:
	vector<CAi*>	m_vAi;

	CAiManager() {m_vAi.reserve(10);}
	~CAiManager() {Shutdown();}
	CAiManager(const CAiManager& rCAiManager) {}
	CAiManager& operator=(const CAiManager& rCAiManager) {}
public:
	static CAiManager* GetInstance();
	int AddBrain(CKartObject* pTarget);
	void NoBrainz();
	CAi* Brain(int nIndex);
	void Think();
	void Shutdown();
};

#endif