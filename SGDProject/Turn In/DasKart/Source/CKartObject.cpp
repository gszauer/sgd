#include "CKartObject.h"
#include "CGamePlayState.h"

CKartObject::CKartObject() : CBase() {
	m_nWeight = 5.0f;
	m_nFriction = 30.0f;
	m_nAccelerationRate = 50.0;
	m_nRotationRate = 3.14f;
	m_nCurSpeed = 0.0f;
	m_nMaxSpeed = 150.0;
	m_nCurRotation = 0.0f;
	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;
	m_nItem = ITEM_MAX;
	m_bAccelerating = false;
	m_bDecelerating = false;
	m_nType = OBJ_KART;
	m_dwSpinoutStart = 0;
	m_bSpinningOut = false;
	m_dwFinishCross = 0;
	m_nAngle = 0;
	m_nOldAngle = 0;	
	m_nLaps = 0;
	m_nTotalAngle = 0;
	m_bCountingAngles = false;
}

void CKartObject::RotateLeft() {
	m_nCurRotation -= m_nRotationRate * CTimer::GetInstance()->GetDelta();
}

void CKartObject::RotateRight() {
	m_nCurRotation += m_nRotationRate * CTimer::GetInstance()->GetDelta();
}

void CKartObject::IncreaseLap() {
	m_nLaps++;
	StartCounting();
}

void CKartObject::Update(float nDelta) {
	if (IsAccelerating()) {
		if (m_nCurSpeed < m_nMaxSpeed) {
			m_nCurSpeed += m_nAccelerationRate * nDelta;
		} else if (m_nCurSpeed > m_nMaxSpeed)
			m_nCurSpeed = m_nMaxSpeed;
	} else if (IsDecelerating()) {
		if (m_nCurSpeed > 0)
			m_nCurSpeed -= m_nAccelerationRate * 2 * nDelta;
	} else {
		if (m_nCurSpeed > 0) {
			m_nCurSpeed -= m_nAccelerationRate * nDelta;
		}
	}

	m_nCurSpeed -= m_nWeight * nDelta;

	static int nBoundaries;
	static int nOffroad;
	static int nEnemieChecks;
	
	nEnemieChecks = CGamePlayState::GetInstance()->NumEnemies();
	nBoundaries = CGamePlayState::GetInstance()->GetBoundaries().size();
	nOffroad = CGamePlayState::GetInstance()->GetOffroad().size();
	
	for (int i = 0; i < nOffroad; ++i) {
		if (CGamePlayState::GetInstance()->GetOffroad()[i].CheckCollision((CBase*)this)) {
			m_nCurSpeed -= m_nFriction * nDelta;
			break;
		}
	}

	if (m_nCurSpeed < 0)
		m_nCurSpeed = 0;
	
	m_vThrust.fX = _ORIENTATION_X_;
	m_vThrust.fY = _ORIENTATION_Y_;
	m_vThrust = Vector2DRotate(m_vThrust, m_nCurRotation);
	m_vThrust = m_vThrust * m_nCurSpeed;

	for (int i = 0; i < nBoundaries; ++i) {
		if (CGamePlayState::GetInstance()->GetBoundaries()[i].CheckCollision((CBase*)this)) {
			m_vThrust = m_vThrust * -5;
			break;
		}
	}

	if (m_bSpinningOut) {
		DWORD dwTick = GetTickCount();
		if (dwTick - m_dwSpinoutStart <= _SPINOUT_MILLIS_) {
			m_vThrust.fX = _ORIENTATION_X_;
			m_vThrust.fY = _ORIENTATION_Y_;

			m_nCurSpeed = 50.0f;

			m_vThrust = Vector2DRotate(m_vThrust, m_nCurRotation + (rand() % 6));
			m_vThrust = m_vThrust * m_nCurSpeed;

			m_vThrust = m_vThrust * (rand() % 3);
			m_vThrust = m_vThrust * -1;

			m_vThrust.fX += rand() % 3 - 6;
			m_vThrust.fY += rand() % 3 - 6;
		} else
			m_bSpinningOut = false;
	}
	
	SetXVelocity(m_vThrust.fX);
	SetYVelocity(m_vThrust.fY);

	CBase::Update(nDelta);

	static RECT rTemp;
	if (IntersectRect(&rTemp, &GetCollisionRect(), &CGamePlayState::GetInstance()->GetFinishRect())) {
		if (GetTickCount() - m_dwFinishCross >= 1000) {
			m_dwFinishCross = GetTickCount();
			IncreaseLap();
		}
	}
	// Needed to figure out direction and distance
	static tVector2D tUpVector = {0, -1};
	static tVector2D tTrackCenter = CGamePlayState::GetInstance()->GetMidPoint();
	static float n180DegreesAsRadians = DEGTORAD(180);
	m_tDifference = GetPositionVector() - tTrackCenter;
	
	// New vector angle
	m_nAngle = AngleBetweenVectors(tUpVector, m_tDifference);
	if (GetXPosition() < tTrackCenter.fX) {	
		m_nAngle = (n180DegreesAsRadians - m_nAngle) + n180DegreesAsRadians;
	}

	// Get out of here if we are not counting
	if (!IsCounting()) {
		m_nOldAngle = m_nAngle;
		return;
	}

	// Do not go in reverse
	if (((m_nAngle - m_nOldAngle) > 0) && (m_nAngle > n180DegreesAsRadians && m_nOldAngle < n180DegreesAsRadians)) {
		m_nOldAngle = m_nAngle;
		// TODO: Send wrong way notification
		return;
	}

	// It has traveled some distance
	if (m_nAngle - m_nOldAngle > 0)
		m_nTotalAngle += m_nAngle - m_nOldAngle;
	m_nOldAngle = m_nAngle;
}

bool CKartObject::CheckCollision(CBase* pBase) {
	
	if (!CBase::CheckCollision(pBase)) 
		return false;

	if (pBase->GetType() == OBJ_KART) {
		RECT rTemp;
		CKartObject* pOponent = (CKartObject*)pBase;
		int nOffset;
		int nEnemOffset;

		RECT rCollision1 = pOponent->GetCollisionRect();
		RECT rCollision2 = this->GetCollisionRect();
		IntersectRect(&rTemp, &rCollision1, &rCollision2);
		nOffset = _MAX_WEIGHT_ - this->GetWeight();
		nEnemOffset = _MAX_WEIGHT_ - pOponent->GetWeight();

		//if (rTemp.bottom - rTemp.top <= rTemp.right - rTemp.left) {
			if (rTemp.bottom >= rCollision2.bottom) { // This was hit from the bottom
				SetYPosition(GetYPosition() - nOffset);
				pOponent->SetYPosition(pOponent->GetYPosition() + nEnemOffset);
				//return true;
			} else if (rTemp.top <= rCollision2.top) { // This was hit from the top
				SetYPosition(GetYPosition() + nOffset);
				pOponent->SetYPosition(pOponent->GetYPosition() - nEnemOffset);
				//return true;
			} 
		//} else {
			if (rTemp.right >= rCollision2.right) { // This gets hit from the right
				SetXPosition(GetXPosition() - nOffset);
				pOponent->SetXPosition(pOponent->GetXPosition() + nEnemOffset);
				//return true;
			} else if (rTemp.left <= rCollision2.left) { // This gets hit from the left
				SetXPosition(GetXPosition() + nOffset);
				pOponent->SetXPosition(pOponent->GetXPosition() - nEnemOffset);
				//return true;
			}
		//}
	} else if (pBase->GetType() == ITEM_GREENSHELL) {
		CGreenShell* pShell = (CGreenShell*)pBase;
		if (pShell->GetOriginObject() != this) {
			CSGD_MessageSystem::GetInstance()->SendMsg(new CKartSpinoutMessage(this));
			CSGD_MessageSystem::GetInstance()->SendMsg(new CDestroyGreenShell(pShell));
		}
	} else if (pBase->GetType() == ITEM_REDSHELL) {
		CRedShell* pShell = (CRedShell*)pBase;
		if (pShell->GetOriginObject() != this) {
			CSGD_MessageSystem::GetInstance()->SendMsg(new CKartSpinoutMessage(this));
			CSGD_MessageSystem::GetInstance()->SendMsg(new CDestroyRedShell(pShell));
		}
	} else if (pBase->GetType() == ITEM_BLUESHELL) {
		CBlueShell* pShell = (CBlueShell*)pBase;
		if (pShell->GetOriginObject() != this) {
			CSGD_MessageSystem::GetInstance()->SendMsg(new CKartSpinoutMessage(this));
			CSGD_MessageSystem::GetInstance()->SendMsg(new CDestroyBlueShell(pShell));
		}
	}

	return true;
}

void CKartObject::Render() {
	static tVector2D vAngle;
	vAngle.fX = _ORIENTATION_X_;
	vAngle.fY = _ORIENTATION_Y_;
	vAngle = Vector2DRotate(vAngle, m_nCurRotation);
	vAngle = vAngle * 10;

	CSGD_Direct3D::GetInstance()->DrawRect(GetCollisionRect(), 255, 0, 0);
	
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition(), GetYPosition(), GetXPosition() + vAngle.fX, GetYPosition() + vAngle.fY, 255, 255, 255);
	static tVector2D tTrackCenter = CGamePlayState::GetInstance()->GetMidPoint();
	//CSGD_Direct3D::GetInstance()->DrawLine(tTrackCenter.fX, tTrackCenter.fY, tTrackCenter.fX + m_tDistance.fX,tTrackCenter.fY + m_tDistance.fY, 0, 0, 0);
}

RECT CKartObject::GetCollisionRect() {
	RECT rTemp;
	SetRect(&rTemp, (GetXPosition() - GetWidth()/2), (GetYPosition() - GetHeight()/2), (GetXPosition() - GetWidth()/2) + GetWidth(), (GetYPosition() - GetHeight()/2) + GetHeight());
	return rTemp;
}

void CKartObject::HandleEvent(CEvent* pEvent) {
	/*if (pEvent->GetEventID() == "kart.spinout") {
		m_bSpinningOut = true;
		m_dwSpinoutStart = GetTickCount();
	}*/
}

void CKartObject::Spinout() {
	m_bSpinningOut = true;
	m_dwSpinoutStart = GetTickCount();
}