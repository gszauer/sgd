#include "CBaseObject.h"

CBase::CBase() {
	SetXPosition(0);
	SetYPosition(0);
	SetXVelocity(0);
	SetYVelocity(0);
	SetWidth(1);
	SetHeight(1);
	SetTextureId(-1);
	SetActive(true);
	AddRef();
	m_nType = OBJ_BASE;
}

void CBase::Update(float nDelta) {
	SetXPosition(GetXPosition() + GetXVelocity() * nDelta);
	SetYPosition(GetYPosition() + GetYVelocity() * nDelta);
}

void CBase::Render() {
	if (GetTextureId() != -1)
		CSGD_TextureManager::GetInstance()->Draw(GetTextureId(), GetXPosition(), GetYPosition());
}

void CBase::AddRef() {
	++m_nRefCount;
}

void CBase::Release() {
	if (--m_nRefCount == 0) 
		delete this;
}

RECT CBase::GetCollisionRect() {
	RECT rTemp;
	SetRect(&rTemp, GetXPosition(), GetYPosition(),  GetXPosition() + GetWidth(), GetYPosition() + GetHeight());
	return rTemp;
}

bool CBase::CheckCollision(CBase* pBase) {
	RECT rTemp;
	return IntersectRect(&rTemp, &GetCollisionRect(), &pBase->GetCollisionRect());
}