#include "CMainMenuState.h"
#include "CGamePlayState.h"
#include "CCreditsState.h"
#include "CGame.h"
#include "CFont.h"

CMainMenuState::CMainMenuState() {
	m_nSelectedItem = 0;
	m_nItemBackground = -1;
	m_bInitialized = false;
	m_nMenuXOffset = 30;
	m_nMenuYOffset = 140;
	m_nMenuBg = -1;

	CMenuItem play("PLAY");
	CMenuItem options("OPTIONS");
	CMenuItem howto("HWOTO");
	CMenuItem scores("SCORES");
	CMenuItem credits("CREDITS");
	CMenuItem exit("EXIT");

	m_vMainMenuItems.push_back(play);
	m_vMainMenuItems.push_back(options);
	m_vMainMenuItems.push_back(howto);
	m_vMainMenuItems.push_back(scores);
	m_vMainMenuItems.push_back(credits);
	m_vMainMenuItems.push_back(exit);

	SetRect(&rActiveItem, 0, 0, 93, 25);
	SetRect(&rInactiveItem, 0, 25, 93, 50);
}
CMainMenuState::~CMainMenuState() {

}
void CMainMenuState::Enter(void) {
	m_pD3D = CSGD_Direct3D::GetInstance();
	m_pDI  = CSGD_DirectInput::GetInstance();
	m_pTM  = CSGD_TextureManager::GetInstance();
	m_pWM  = CSGD_WaveManager::GetInstance();
	m_pDS  = CSGD_DirectSound::GetInstance();

	if (!m_bInitialized) {
		int nActiveSound = m_pWM->LoadWave("Resource/Sounds/gas_ActivateMenuItem.wav");
		int nActiveFont = m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png");
		int nInactiveFont = m_pTM->LoadTexture("Resource/Images/gas_InactiveMenuItem.png");
		m_nItemBackground = m_pTM->LoadTexture("Resource/Images/gas_MenuItemBg.png");
		m_nMenuBg = m_pTM->LoadTexture("Resource/Images/gas_MainMenuBg.jpg");
		for (int i = 0; i < m_vMainMenuItems.size(); ++i) {
			m_vMainMenuItems[i].Initialize(nActiveSound, nActiveFont, nInactiveFont, m_nMenuXOffset, m_nMenuYOffset + (i * 45));
			
		}
		m_bInitialized = true;
	}
	for (int i = 0; i < m_vMainMenuItems.size(); ++i)
		m_vMainMenuItems[i].SetInactive();
	m_vMainMenuItems[m_nSelectedItem].SetActive();
}
bool CMainMenuState::Input(void) {
	if (m_pDI->KeyPressed(DIK_UP) || m_pDI->KeyPressed(DIK_W)) {
		m_vMainMenuItems[m_nSelectedItem].SetInactive();
		if (--m_nSelectedItem < 0)
			m_nSelectedItem = MAIN_MENU_MAX - 1;
		m_vMainMenuItems[m_nSelectedItem].SetActive();
		return true;
	}
	if (m_pDI->KeyPressed(DIK_DOWN) || m_pDI->KeyPressed(DIK_S)) {
		m_vMainMenuItems[m_nSelectedItem].SetInactive();
		if (++m_nSelectedItem  >= MAIN_MENU_MAX)
			m_nSelectedItem = 0;
		m_vMainMenuItems[m_nSelectedItem].SetActive();
		return true;
	}
	if (m_pDI->KeyPressed(DIK_RETURN)) {

		switch (m_nSelectedItem) {
			case MAIN_MENU_PLAY: {
				CGame::GetInstance()->ChangeState((IGameState*)CGamePlayState::GetInstance());
								 } break;
			case MAIN_MENU_OPTIONS: {
				//CGame::GetInstance()->ChangeState((IGameState*)CGameOptions::GetInstance());
									} break;
			case MAIN_MENU_HOWTO: {
				//CGame::GetInstance()->ChangeState((IGameState*)CHowToScreen::GetInstance());
								  } break;
			case MAIN_MENU_CREDITS: {
				CGame::GetInstance()->ChangeState((IGameState*)CCreditsState::GetInstance());
									} break;
			case MAIN_MENU_EXIT: {
				return false;
								 } break;
		}
	}
	return true;
}
void CMainMenuState::Update(float fDelta) {

}
void CMainMenuState::Render(void) {
	m_pTM->Draw(m_nMenuBg, 0, 0);
	CFont::GetInstance()->Render("Main Menu", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 370, 80, 1.5f, 1.5f);
	int nSize = m_vMainMenuItems.size();
	for (int i = 0; i < nSize; ++i) {
		m_pTM->Draw(m_nItemBackground, m_nMenuXOffset + 85 + (93 / 2), m_nMenuYOffset - 5 + (i * 45), -1.5f, 1.0f, ((m_nSelectedItem == i)? &rActiveItem : &rInactiveItem));
		m_vMainMenuItems[i].Render();
	}	
}
void CMainMenuState::Exit(void) {

}

CMainMenuState* CMainMenuState::GetInstance() {
	static CMainMenuState mainMenuState;
	mainMenuState.m_nSelectedItem = 0;
	return &mainMenuState;
}