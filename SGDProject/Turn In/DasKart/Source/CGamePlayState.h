#ifndef _H_CGAMEPLAYSTATE_
#define _H_CGAMEPLAYSTATE_

#include "CGame.h"
#include "CTimer.h"
#include "IGameState.h"
#include "CAi.h"
#include "CKartManager.h"
#include "CGreenShell.h"
#include "CBlueShell.h"
#include "CKartObject.h"
#include "CBoundaryObject.h"
#include "COffroadObject.h"
#include "SGDWrappers/CSGD_Direct3D.h"
#include "SGDWrappers/CSGD_ObjectManager.h"
#include <vector>
using std::vector;

#define _NUM_ENEMIES_ 7

class CGamePlayState : public IGameState {
private:
	unsigned char			m_ucAccelerate;
	unsigned char			m_ucDecelerate;
	unsigned char			m_ucLeft;
	unsigned char			m_ucRight;
	unsigned char			m_ucItem;
	CKartObject*			m_pPlayer;
	int						m_nPlayerRank;
	tVector2D				m_vTrackCenterPoint;
	RECT					m_rStartLine;
	RECT					rScreen;
	vector<CBoundaryObject> m_vBoundarys;
	vector<COffroadObject>	m_vOffroad;
	
	CGamePlayState();
	~CGamePlayState() {}
	CGamePlayState(const CGamePlayState& rCGamePlayState) {}
	CGamePlayState& operator=(const CGamePlayState& rCGamePlayState) {}
public:
	static CGamePlayState* GetInstance();
	vector<CBoundaryObject>& GetBoundaries(); // TODO: Replace
	vector<COffroadObject>& GetOffroad(); // TODO: Replace
	CKartObject* GetEnemies(); // TODO: Replace
	CKartObject* GetPlayer() {return m_pPlayer;}
	int NumEnemies() { return _NUM_ENEMIES_; } // TODO: Remove
	RECT GetFinishRect() {return m_rStartLine;}
	tVector2D GetMidPoint() {return m_vTrackCenterPoint;}
	CKartObject* GetNextKart();
	void Initialize();
	void Reset();
	void Enter();
	bool Input();
	void Update(float fDelta);
	void Render();
	void Exit();
};

#endif