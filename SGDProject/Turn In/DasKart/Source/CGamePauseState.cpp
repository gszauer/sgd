#include "CGamePauseState.h"
#include "CMainMenuState.h"
#include "CGamePlayState.h"
#include "CGame.h"

CGamePauseState::CGamePauseState() {
	m_nSelectedItem = 0;
	m_nMenuXOffset = 235;
	m_nMenuYOffset = 225;
	m_bInitialized = false;
	m_nItemBackground = false;

	CMenuItem resume("RESUME");
	CMenuItem menu("EXIT");

	m_vPauseMenuItems.push_back(resume);
	m_vPauseMenuItems.push_back(menu);

	SetRect(&rActiveItem, 0, 0, 93, 25);
	SetRect(&rInactiveItem, 0, 25, 93, 50);
}
CGamePauseState::~CGamePauseState() {

}
void CGamePauseState::Enter(void) {
	m_pD3D = CSGD_Direct3D::GetInstance();
	m_pDI  = CSGD_DirectInput::GetInstance();
	m_pTM  = CSGD_TextureManager::GetInstance();
	m_pWM  = CSGD_WaveManager::GetInstance();
	m_pDS  = CSGD_DirectSound::GetInstance();

	if (!m_bInitialized) {
		int nActiveSound = m_pWM->LoadWave("Resource/Sounds/gas_ActivateMenuItem.wav");
		int nActiveFont = m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png");
		int nInactiveFont = m_pTM->LoadTexture("Resource/Images/gas_InactiveMenuItem.png");
		m_nItemBackground = m_pTM->LoadTexture("Resource/Images/gas_MenuItemBg.png");
		int m_nSize = m_vPauseMenuItems.size();
		for (int i = 0; i < m_nSize; ++i) {
			m_vPauseMenuItems[i].Initialize(nActiveSound, nActiveFont, nInactiveFont, m_nMenuXOffset, m_nMenuYOffset + (i * 45));
		}
		m_bInitialized = true;
	}
	m_vPauseMenuItems[m_nSelectedItem].SetActive();
}
bool CGamePauseState::Input(void) {
	if (m_pDI->KeyPressed(DIK_UP) || m_pDI->KeyPressed(DIK_W)) {
		m_vPauseMenuItems[m_nSelectedItem].SetInactive();
		if (--m_nSelectedItem < 0)
			m_nSelectedItem = PAUSE_MENU_MAX - 1;
		m_vPauseMenuItems[m_nSelectedItem].SetActive();
		return true;
	}
	if (m_pDI->KeyPressed(DIK_DOWN) || m_pDI->KeyPressed(DIK_S)) {
		m_vPauseMenuItems[m_nSelectedItem].SetInactive();
		if (++m_nSelectedItem  >= PAUSE_MENU_MAX)
			m_nSelectedItem = 0;
		m_vPauseMenuItems[m_nSelectedItem].SetActive();
		return true;
	}
	if (m_pDI->KeyPressed(DIK_RETURN)) {
		switch (m_nSelectedItem) {
			case PAUSE_MENU_RESUME: {
				CGame::GetInstance()->ChangeState((IGameState*)CGamePlayState::GetInstance());
									} break;
			case PAUSE_MENU_EXIT: {
				CGamePlayState::GetInstance()->Reset();
				CGame::GetInstance()->ChangeState((IGameState*)CMainMenuState::GetInstance());
								  } break;
		}
	}

	if (CSGD_DirectInput::GetInstance()->KeyPressed(DIK_ESCAPE) || CSGD_DirectInput::GetInstance()->KeyPressed(DIK_P)) {
		CGame::GetInstance()->ChangeState((IGameState*)CGamePlayState::GetInstance());
		return true;
	}

	return true;
}
void CGamePauseState::Update(float nDelta) {

}
void CGamePauseState::Render(void) {
	CFont::GetInstance()->Render("Game Paused", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 180, 20, 1.5f, 1.5f);
	int nSize = m_vPauseMenuItems.size();
	for (int i = 0; i < nSize; ++i) {
		m_pTM->Draw(m_nItemBackground, m_nMenuXOffset + 130, m_nMenuYOffset - 5 + (i * 45), -1.5f, 1.0f, ((m_nSelectedItem == i)? &rActiveItem : &rInactiveItem));
		m_vPauseMenuItems[i].Render();
	}
	CGamePlayState::GetInstance()->Render();
}
void CGamePauseState::Exit(void) {

}

CGamePauseState* CGamePauseState::GetInstance() {
	static CGamePauseState pauseState;
	pauseState.m_nSelectedItem = 0;
	return &pauseState;
}