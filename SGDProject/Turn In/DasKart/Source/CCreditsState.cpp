#include "CCreditsState.h"

CCreditsState::CCreditsState() {
	m_bInitialized = false;
	m_nMenuBg = -1;
}

CCreditsState::~CCreditsState() {}

void CCreditsState::Enter(void) {
	m_pD3D = CSGD_Direct3D::GetInstance();
	m_pDI  = CSGD_DirectInput::GetInstance();
	m_pTM  = CSGD_TextureManager::GetInstance();
	m_pWM  = CSGD_WaveManager::GetInstance();
	m_pDS  = CSGD_DirectSound::GetInstance();

	if (!m_bInitialized) {
		int nActiveFont = m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png");
		m_nMenuBg = m_pTM->LoadTexture("Resource/Images/gas_MainMenuBg.jpg");
		m_bInitialized = true;
	}
}

bool CCreditsState::Input(void) {
	if (CSGD_DirectInput::GetInstance()->KeyPressed(DIK_ESCAPE)) {
		CGame::GetInstance()->ChangeState((IGameState*)CMainMenuState::GetInstance());
		return true;
	}
	return true;
}

void CCreditsState::Update(float fDelta) {

}

void CCreditsState::Render(void) {
	m_pTM->Draw(m_nMenuBg, 0, 0);
	CFont::GetInstance()->Render("Credits", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 370, 80, 1.5f, 1.5f);
	
	CFont::GetInstance()->Render("Main Programmer", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 100, 1.0f, 1.0f);
	CFont::GetInstance()->Render("Gabor Szauer", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 120, 1.0f, 1.0f);

	CFont::GetInstance()->Render("Project Supervisor", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 160, 1.0f, 1.0f);
	CFont::GetInstance()->Render("David Brown", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 180, 1.0f, 1.0f);

	CFont::GetInstance()->Render("Special Thanks To", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 220, 1.0f, 1.0f);
	CFont::GetInstance()->Render("Flashkit", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 240, 1.0f, 1.0f);
	CFont::GetInstance()->Render("The Spriters Resource", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 260, 1.0f, 1.0f);
	CFont::GetInstance()->Render("Google", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 280, 1.0f, 1.0f);
	CFont::GetInstance()->Render("Redbull", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 300, 1.0f, 1.0f);

	CFont::GetInstance()->Render("Press Escape to exit", m_pTM->LoadTexture("Resource/Images/gas_ActiveMenuItem.png"), 20, 400, 1.5f, 1.5f);
}

void CCreditsState::Exit(void) {

}

CCreditsState* CCreditsState::GetInstance() {
	static CCreditsState creditStateNotScore;
	return &creditStateNotScore;
}