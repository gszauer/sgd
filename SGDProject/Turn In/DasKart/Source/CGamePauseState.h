#ifndef _H_CGAMEPAUSESTATE_
#define _H_CGAMEPAUSESTATE_

#include "IGameState.h"
#include "CMenuItem.h"
#include "SGDWrappers.h"
#include <windows.h>

enum PauseMenuItems {PAUSE_MENU_RESUME, PAUSE_MENU_EXIT, PAUSE_MENU_MAX};

class CGamePauseState : public IGameState {
private:
	CGamePauseState();
	~CGamePauseState();
	CGamePauseState(const CGamePauseState&) {}
	CGamePauseState& operator=(const CGamePauseState&) {}

	CSGD_Direct3D*		 m_pD3D;
	CSGD_DirectInput*	 m_pDI;
	CSGD_TextureManager* m_pTM;
	CSGD_WaveManager*	 m_pWM;
	CSGD_DirectSound*	 m_pDS;

	vector<CMenuItem> m_vPauseMenuItems;
	int m_nSelectedItem;
	RECT rActiveItem;
	RECT rInactiveItem;

	int m_nMenuXOffset;
	int m_nMenuYOffset;
	bool m_bInitialized;
	int m_nItemBackground;
public:
	void Enter(void);
	bool Input(void);
	void Update(float nDelta);
	void Render(void);
	void Exit(void);
	static CGamePauseState* GetInstance();
};

#endif