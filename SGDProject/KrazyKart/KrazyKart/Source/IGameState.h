#ifndef _H_IGAMESTATE_
#define _H_IGAMESTATE_

#include "SGDWrappers.h"

class IGameState {
public:
	virtual void Enter(void) = 0;  // Enters the Game State
	virtual bool Input(void) = 0;  // Input
	virtual void Update(void) = 0; // Update
	virtual void Render(void) = 0; // Draw
	virtual void Exit(void) = 0;   // Leaves the Game State
	virtual ~IGameState() = 0 {};	// Destructor
};

#endif