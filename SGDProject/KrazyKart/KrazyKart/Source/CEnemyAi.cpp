#include "CEnemyAi.h"
#include "SGDWrappers/CSGD_Direct3D.h"

void CAi::Seek(int m_nTargetX, int m_nTargetY) {
	m_vTarget.x = m_nTargetX;
	m_vTarget.y = m_nTargetY;

	Vector2D vCurPos;
	vCurPos.x = m_nXPosition;
	vCurPos.y = m_nYPosition;

	Vector2D vDesiredVelocity = Vec2DNormalize(m_vTarget - vCurPos) * m_nMaxSpeed;
	
	m_nXVelocity = vDesiredVelocity.x;
	m_nYVelocity = vDesiredVelocity.y;
}

void CAi::FollowPath(CPath* pPath) {
	Vector2D vCurPos;
	vCurPos.x = m_nXPosition;
	vCurPos.y = m_nYPosition;
	if (Vec2DDistanceSq(pPath->CurrentWaypoint(), vCurPos) < 2) {
		pPath->SetNextWaypoint();
	}

	if (!pPath->Finished()) {
		Seek(pPath->CurrentWaypoint().x, pPath->CurrentWaypoint().y);
	}
}

CAi::CAi() {
	m_nXPosition = 0;
	m_nYPosition = 0;
	m_nXVelocity = 0;
	m_nYVelocity = 0;
	m_nRefCount = 1;
	m_nMaxSpeed = 2.0f;
}

CAi::~CAi() {}

RECT CAi::GetCollisionRect() {
	RECT rTemp;
	SetRect(&rTemp, m_nXPosition, m_nYPosition, m_nXPosition + m_nWidth, m_nYPosition + m_nHeight);
	return rTemp;
}

void CAi::Update() {
	SetXPosition(GetXPosition() + GetXVelocity());
	SetYPosition(GetYPosition() + GetYVelocity());
}

void CAi::Render() {
	CSGD_Direct3D::GetInstance()->LineBegin();
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition() - 5, GetYPosition() - 5, GetXPosition() + 5, GetYPosition() + 5, 0, 255, 0);
	CSGD_Direct3D::GetInstance()->DrawLine(GetXPosition() + 5, GetYPosition() - 5, GetXPosition() - 5, GetYPosition() + 5, 0, 255, 0);
	CSGD_Direct3D::GetInstance()->LineEnd();
}

//bool CAi::CheckCollision(CBase* pBase) {
//	RECT tRect;
//	return IntersectRect(&tRect, &GetCollisionRect(), &pBase->GetCollisionRect());
//}
