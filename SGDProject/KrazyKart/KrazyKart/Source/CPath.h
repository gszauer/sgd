#ifndef _H_CPATH_
#define _H_CPATH_

#include <list>
#include <cassert>
#include "SGDWrappers/CSGD_Direct3D.h"
#include "Vector2D.h"
#include <vector>
using std::vector;

class CPath {
private:
	std::vector<Vector2D> m_vWayPoints;
	int m_nCurWayPoint;
	bool m_bLooping;
public:
	CPath() :m_bLooping(true) {
		m_nCurWayPoint = 0;
	}
	CPath(int nNumWayPoints, double nMinX, double nMinY, double nMaxX, double nMaxY, bool bLooping):m_bLooping(bLooping) {
		CreateRandomPath(nNumWayPoints, nMinX, nMinY, nMaxX, nMaxY);
		m_nCurWayPoint = 0;
	}
	Vector2D CurrentWaypoint() const {
		return m_vWayPoints[m_nCurWayPoint];
	}
	bool Finished() const {
		return (m_nCurWayPoint >= m_vWayPoints.size());
	}
	inline void SetNextWaypoint();
	vector<Vector2D> CreateRandomPath(int nNumWayPoints, double nMinX, double nMinY, double nMaxX, double nMaxY);
	void LoopOn(){m_bLooping = true;}
	void LoopOff(){m_bLooping = false;}
	void AddWayPoint(Vector2D new_point);
	//void Set(std::list<Vector2D> new_path){m_WayPoints = new_path;curWaypoint = m_WayPoints.begin();}
	//void Set(const Path& path){m_WayPoints=path.GetPath(); curWaypoint = m_WayPoints.begin();}
	void Clear() {m_vWayPoints.clear();}
	vector<Vector2D> GetPath()const{return m_vWayPoints;}
	void Render() const; 
};

inline void CPath::SetNextWaypoint() {
	if (++m_nCurWayPoint == m_vWayPoints.size()) {
		if (m_bLooping) {
			m_nCurWayPoint = 0; 
		}
	}
}  

#endif