#ifndef _H_FONT_
#define _H_FONT_

#include <windows.h>
#include <cstring>
#include "SGDWrappers/CSGD_TextureManager.h"

class CFont {
private:
	char m_cStartingChar; // space
	int m_nCharWidth;
	int m_nCharHeight;
	int m_nCharCols;
	RECT CellAlgorightm(int m_nId) {
		RECT rCell;
		rCell.left   = (m_nId % m_nCharCols) * m_nCharWidth;
		rCell.top    = (m_nId / m_nCharCols) * m_nCharHeight;
		rCell.right  = rCell.left + m_nCharWidth;
		rCell.bottom = rCell.top + m_nCharHeight;
		return rCell;
	}
	CFont() {}
	CFont(const CFont& rCGame) {}
	CFont& operator=(const CFont& rCGame){}
	~CFont() {}
public:
	static CFont* GetInstance() {
		static CFont font;
		return &font;
	}

	void Initialize(char cStartChar, int nCharWidth, int nCharHeight, int nCharCols) {
		m_cStartingChar = cStartChar;
		m_nCharWidth = nCharWidth;
		m_nCharHeight = nCharHeight;
		m_nCharCols = nCharCols;
	}

	void Render(const char* szRender, int nFontId = -1, int nX = 0, int nY = 0, float nScalseX = 1.0f, float nScaleY = 1.0f) {
		int nSize = strlen(szRender);
		int nStart = nX;
		char cTemp;
		int nTemp;
		RECT rTemp;
		for (int i = 0; i < nSize; ++i) {
			cTemp = toupper(szRender[i]);

			if (cTemp == ' ') {
				nX += m_nCharWidth * nScalseX;
				// TODO: Wrap text
				continue;
			} else if (cTemp == '\n') {
				nY += m_nCharHeight * nScaleY;
				nX = nStart;
				continue;
			}

			nTemp = (int)cTemp - (int)m_cStartingChar;
			rTemp = CellAlgorightm(nTemp);
			if (nFontId != -1)
				CSGD_TextureManager::GetInstance()->Draw(nFontId, nX, nY, nScalseX, nScalseX, &rTemp);
			nX += m_nCharWidth * nScalseX;
		}
	}
};
#endif