#ifndef _H_MENU_ITEM_
#define _H_MENU_ITEM_

#include <cstring>
#include "Font.h"

class CMenuItem {
private:
	bool m_bIsActive;
	char* m_szItemName;
	int m_nActiveFont;
	int m_nInactiveFont;
	int m_nActivateSound;
	int m_nX;
	int m_nY;

	CMenuItem() {}
	void Set(const char* szItemName, bool bIsActive, int m_nActiveFont, int nInactiveFont, int nActiveSound, int nX, int nY) {
		int nStrLen = strlen(szItemName) + 1;
		m_szItemName = new char[nStrLen];
		strcpy(m_szItemName, szItemName);
		m_bIsActive = bIsActive;
		m_nActivateSound = nActiveSound;
		m_nActiveFont = nInactiveFont;
		m_nInactiveFont = nInactiveFont;
		m_nX = nX;
		m_nY = nY;
	}
public:
	CMenuItem(const char* szItemName) {
		Set(szItemName, false, -1, -1, -1, 0, 0);
	}
	
	CMenuItem(const CMenuItem& rMenuItem) {
		m_szItemName = NULL;
		Set(rMenuItem.m_szItemName, rMenuItem.m_bIsActive, rMenuItem.m_nActiveFont, rMenuItem.m_nInactiveFont, rMenuItem.m_nActivateSound, rMenuItem.m_nX, rMenuItem.m_nY);
	}

	CMenuItem& operator=(const CMenuItem& rMenuItem) {
		if (this != &rMenuItem) {
			Set(rMenuItem.m_szItemName, rMenuItem.m_bIsActive, rMenuItem.m_nActiveFont, rMenuItem.m_nInactiveFont, rMenuItem.m_nActivateSound, rMenuItem.m_nX, rMenuItem.m_nY);
		}
		return *this;
	}
	
	~CMenuItem() {
		delete[] m_szItemName;
	}

	void Initialize(int nActiveSound, int nActiveFont, int nInactiveFont, int nX, int nY) {
		m_nActivateSound = nActiveSound;
		m_nActiveFont = nActiveFont;
		m_nInactiveFont = nInactiveFont;
		m_nX = nX;
		m_nY = nY;
	}
	void ToggleActive() {
		m_bIsActive = !m_bIsActive;
		if (m_bIsActive)
			CSGD_WaveManager::GetInstance()->Play(m_nActivateSound);
	}
	void SetActive() {
		m_bIsActive = true;
		CSGD_WaveManager::GetInstance()->Play(m_nActivateSound);
	}
	void SetInactive() {
		m_bIsActive = false;
	}
	void Render() {
		CFont::GetInstance()->Render(m_szItemName, ((m_bIsActive)? m_nActiveFont : m_nInactiveFont), m_nX, m_nY);
	}
};
#endif