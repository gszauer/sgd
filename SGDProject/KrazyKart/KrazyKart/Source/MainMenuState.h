#ifndef _H_GAMEMENUSTATE_
#define _H_GAMEMENUSTATE_

#include "IGameState.h"
#include <windows.h>
#include "MenuItem.h"

enum MainMenuItems {MAIN_MENU_PLAY, MAIN_MENU_OPTIONS, MAIN_MENU_HOWTO, MAIN_MENU_CREDITS, MAIN_MENU_EXIT, MAIN_MENU_MAX};

class CMainMenuState : public IGameState {
private:
	CMainMenuState();
	~CMainMenuState();
	CMainMenuState(const CMainMenuState&) {}
	CMainMenuState& operator=(const CMainMenuState&) {}

	CSGD_Direct3D*		 m_pD3D;
	CSGD_DirectInput*	 m_pDI;
	CSGD_TextureManager* m_pTM;
	CSGD_WaveManager*	 m_pWM;
	CSGD_DirectSound*	 m_pDS;

	vector<CMenuItem> m_vMainMenuItems;
	int m_nSelectedItem;
	int m_nItemBackground;
	int m_nMenuBg;
	int m_nMenuXOffset;
	int m_nMenuYOffset;
	bool m_bInitialized;
	RECT rActiveItem;
	RECT rInactiveItem;
public:
	void Enter(void);
	bool Input(void);
	void Update(void);
	void Render(void);
	void Exit(void);
	static CMainMenuState* GetInstance();
};

#endif