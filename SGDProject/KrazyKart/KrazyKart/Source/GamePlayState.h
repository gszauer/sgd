#ifndef _H_GAMEPLAYSTATE_
#define _H_GAMEPLAYSTATE_

#include "IGameState.h"
#include "CPath.h"
#include "CEnemyAi.h"

class CGamePlayState : public IGameState {
private:
	CGamePlayState();
	~CGamePlayState();
	CGamePlayState(const CGamePlayState&) {}
	CGamePlayState& operator=(const CGamePlayState&) {}

	CSGD_Direct3D*		 m_pD3D;
	CSGD_DirectInput*	 m_pDI;
	CSGD_TextureManager* m_pTM;
	CSGD_WaveManager*	 m_pWM;
	CSGD_DirectSound*	 m_pDS;

	CPath m_path;
	CAi m_enemy[4];
public:
	void Enter(void);
	bool Input(void);
	void Update(void);
	void Render(void);
	void Exit(void);
	static CGamePlayState* GetInstance();
	static void ResetGame();
};

#endif