#include "GamePlayState.h"
#include "GamePauseState.h"
#include "CGame.h"

CGamePlayState::CGamePlayState() {
	m_enemy[0].SetXPosition(0);
	m_enemy[0].SetYPosition(0);
	m_enemy[1].SetXPosition(640);
	m_enemy[1].SetYPosition(0);
	m_enemy[2].SetXPosition(0);
	m_enemy[2].SetYPosition(480);
	m_enemy[3].SetXPosition(640);
	m_enemy[3].SetYPosition(480);
	for (int i = 0; i < 4; ++i) {
		m_enemy[i].SetXVelocity(0);
		m_enemy[i].SetYVelocity(0);
	}
	m_path.CreateRandomPath(6, 100, 100, 540, 380);
}
void CGamePlayState::ResetGame() {
	GetInstance()->m_enemy[0].SetXPosition(0);
	GetInstance()->m_enemy[0].SetYPosition(0);
	GetInstance()->m_enemy[1].SetXPosition(640);
	GetInstance()->m_enemy[1].SetYPosition(0);
	GetInstance()->m_enemy[2].SetXPosition(0);
	GetInstance()->m_enemy[2].SetYPosition(480);
	GetInstance()->m_enemy[3].SetXPosition(640);
	GetInstance()->m_enemy[3].SetYPosition(480);
	for (int i = 0; i < 4; ++i) {
		GetInstance()->m_enemy[i].SetXVelocity(0);
		GetInstance()->m_enemy[i].SetYVelocity(0);
	}
	GetInstance()->m_path.CreateRandomPath(6, 100, 100, 540, 380);
}

CGamePlayState::~CGamePlayState() {

}
void CGamePlayState::Enter(void) {
	m_pD3D = CSGD_Direct3D::GetInstance();
	m_pDI  = CSGD_DirectInput::GetInstance();
	m_pTM  = CSGD_TextureManager::GetInstance();
	m_pWM  = CSGD_WaveManager::GetInstance();
	m_pDS  = CSGD_DirectSound::GetInstance();
}
bool CGamePlayState::Input(void) {
	if (m_pDI->KeyPressed(DIK_ESCAPE) || m_pDI->KeyPressed(DIK_P)) {
		CGame::GetInstance()->ChangeState((IGameState*)CGamePauseState::GetInstance());
	}
	return true;
}
void CGamePlayState::Update(void) {
	for (int i = 0; i < 4; ++i) {
		m_enemy[i].FollowPath(&m_path);
		m_enemy[i].Update();
	}
}
void CGamePlayState::Render(void) {
	m_path.Render();
	for (int i = 0; i < 4; ++i)
		m_enemy[i].Render();
}
void CGamePlayState::Exit(void) {

}

CGamePlayState* CGamePlayState::GetInstance() {
	static CGamePlayState gamePlay;
	return &gamePlay;
}