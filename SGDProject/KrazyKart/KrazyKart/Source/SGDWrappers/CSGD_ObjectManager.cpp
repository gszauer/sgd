////////////////////////////////////////////////
//	File	:	"CSGD_ObjectManager.cpp"
//
//	Author	:	David Brown (DB)
//
//	Purpose	:	To contain and manage all of our game objects.
/////////////////////////////////////////////////
#include "CSGD_ObjectManager.h"
#include "../CBase.h"
CSGD_ObjectManager::CSGD_ObjectManager(void)
{
}

CSGD_ObjectManager::~CSGD_ObjectManager(void)
{
}

CSGD_ObjectManager* CSGD_ObjectManager::GetInstance() {
	static CSGD_ObjectManager m_pSelf;
	// TODO: Nothing?
	return &m_pSelf;
}

void CSGD_ObjectManager::UpdateObjects(float fDelta)
{
	vector<CBase*>::iterator iter = m_vObjectList.begin();

	while(iter != m_vObjectList.end())
	{
		if ((*iter)->IsActive())
		{
			(*iter)->Update(fDelta);
		} 
		else
		{
			m_vDeadObjects.push_back(*iter);
		}
		iter++;
	}
	for(unsigned int i = 0; i < m_vDeadObjects.size(); i++)
	{
		RemoveObject(m_vDeadObjects[i]);
	}

	
}

void CSGD_ObjectManager::RenderObjects(void)
{
	for (unsigned int i=0; i < m_vObjectList.size(); i++)
		m_vObjectList[i]->Render();
}

void CSGD_ObjectManager::CheckCollisions() {
	for (unsigned int i = 0; i < m_vObjectList.size(); ++i) {
		for (unsigned int n = 0; n < m_vObjectList.size(); ++n) {
			if (m_vObjectList[i]->GetType() == m_vObjectList[n]->GetType())
				continue;
			if(m_vObjectList[i]->CheckCollision(m_vObjectList[n]))
				break;
		}
	}
}

void CSGD_ObjectManager::AddObject(CBase* pObject)
{
	//	Check for valid object pointer
	if (pObject == NULL)
		return;

	//	Add object to object list
	m_vObjectList.push_back(pObject);

	//	Add my reference to it.
	pObject->AddRef();
}

void CSGD_ObjectManager::RemoveObject(CBase* pObject)
{
	//	Check for valid object pointer
	if (pObject == NULL)
		return;

	for (vector<CBase*>::iterator iter = m_vObjectList.begin();
		 iter != m_vObjectList.end();
		 iter++)
	{
		// if this is the object we are looking for.
		if ((*iter) == pObject)
		{
			// Remove my reference to this object.
			(*iter)->Release();

			// Remove the object from the list
			iter = m_vObjectList.erase(iter);
			break;
		}
	}
}

void CSGD_ObjectManager::RemoveAllObjects(void)
{
	//	Call Release() on all objects.
	for (unsigned int i=0; i < m_vObjectList.size(); i++)
	{
		m_vObjectList[i]->Release();
	}

	//	Collapse the vector
	m_vObjectList.clear();
}